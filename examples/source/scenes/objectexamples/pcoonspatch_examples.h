#ifndef SCENES_OBJECTEXAMPLES_PCOONSPATCH_EXAMPLES_H
#define SCENES_OBJECTEXAMPLES_PCOONSPATCH_EXAMPLES_H

// gmlib2
#include <gmlib2/qt/scenegraph.h>
#include <gmlib2/qt/exampleobjects/parametric/parametricobjects.h>

namespace examples
{

  // Declare contained examples
  void initPBilinearCoonsPatchExample(gmlib2::qt::Scenegraph* scenegraph);
  void initPBicubicCoonsPatchExample(gmlib2::qt::Scenegraph* scenegraph);


  // "Local" detail resources
  namespace detail
  {
    auto lambda_construct_my_very_special_thing = []() {};

    class ZeroDerCurve : public gmlib2::parametric::PCurve<gmlib2::qt::SceneObject, 1> {
      using Base = gmlib2::parametric::PCurve<gmlib2::qt::SceneObject, 1>;

    public:
      // Types
      using Unit_Type        = typename Base::Unit_Type;
      using EvaluationResult = typename Base::EvaluationResult;
      using Point            = typename Base::Point_Type;
      using Vector           = typename Base::Vector_Type;
      using HVector          = typename Base::VectorH_Type;
      // Paramter space types
      static constexpr auto PVectorDim = Base::PSpace_VectorDim;
      using PSpacePoint                = typename Base::PSpace_Point_Type;
      using PSizeArray                 = typename Base::PSpace_SizeArray_Type;
      using PBoolArray                 = typename Base::PSpace_BoolArray_Type;

      // Constructor
      using Base::Base;

      // PCurve interface
    public:
      PBoolArray isClosed() const override { return {{false}}; }
      PSpacePoint    startParameters() const override { return PSpacePoint{0}; }
      PSpacePoint    endParameters() const override { return PSpacePoint{1}; }

    protected:
      EvaluationResult evaluate(const PSpacePoint& /*par*/,
                                const PSizeArray& /*no_der*/,
                                const PBoolArray& /*from_left*/
                                ) const override
      {
        return HVector{0};
      }
    };

  }   // namespace detail


  // Examples
  void initPBilinearCoonsPatchExample(gmlib2::qt::Scenegraph* scenegraph)
  {
    using namespace gmlib2::qt::parametric;

    // Bilinear Coons patch
    auto* c0 = new PHermiteCurveP2V2(PHermiteCurveP2V2::Point{-4.0, 0.0, -4.0},
                                     PHermiteCurveP2V2::Point{4.0, 0.0, -4.0},
                                     PHermiteCurveP2V2::Vector{8.0, 0.0, 0.0},
                                     PHermiteCurveP2V2::Vector{8.0, 0.0, 0.0});
    c0->initDefaultComponents();
    c0->defaultMesh()->setSamples(100);

    auto* c1 = new PHermiteCurveP2V2(PHermiteCurveP2V2::Point{-4.0, 0.0, 4.0},
                                     PHermiteCurveP2V2::Point{4.0, 0.0, 4.0},
                                     PHermiteCurveP2V2::Vector{8.0, -20.0, 0.0},
                                     PHermiteCurveP2V2::Vector{8.0, 0.0, 0.0});
    c1->initDefaultComponents();
    c1->defaultMesh()->setSamples(100);

    auto* g0 = new PHermiteCurveP2V2(PHermiteCurveP2V2::Point{-4.0, 0.0, -4.0},
                                     PHermiteCurveP2V2::Point{-4.0, 0.0, 4.0},
                                     PHermiteCurveP2V2::Vector{0.0, 0.0, 8.0},
                                     PHermiteCurveP2V2::Vector{0.0, 0.0, 8.0});
    g0->initDefaultComponents();
    g0->defaultMesh()->setSamples(100);

    auto* g1 = new PHermiteCurveP2V2(PHermiteCurveP2V2::Point{4.0, 0.0, -4.0},
                                     PHermiteCurveP2V2::Point{4.0, 0.0, 4.0},
                                     PHermiteCurveP2V2::Vector{0.0, 8.0, 20.0},
                                     PHermiteCurveP2V2::Vector{0.0, 8.0, 8.0});
    g1->initDefaultComponents();
    g1->defaultMesh()->setSamples(100);

    auto* coons_patch = new PBilinearCoonsPatch(c0, c1, g0, g1, scenegraph);
    c0->setParent(coons_patch);
    c1->setParent(coons_patch);
    g0->setParent(coons_patch);
    g1->setParent(coons_patch);
    coons_patch->initDefaultComponents();
    coons_patch->defaultMesh()->setSamples({20, 20});
  }


  void initPBicubicCoonsPatchExample(gmlib2::qt::Scenegraph* scenegraph)
  {
    using namespace gmlib2::qt::parametric;

    // clang-format off
  auto* c0 = new PHermiteCurveP2V2(
    PHermiteCurveP2V2::Point{-4.0, 0.0, -4.0},
    PHermiteCurveP2V2::Point{-4.0, 0.0, 4.0},
//    PHermiteCurveP2V2::Vector{0.0, -20.0, 8.0},
    PHermiteCurveP2V2::Vector{0.0, 0.0, 8.0},
    PHermiteCurveP2V2::Vector{0.0, 0.0, 8.0});
    // clang-format on
    c0->initDefaultComponents();
    c0->defaultMesh()->setSamples(100);

    // clang-format off
  auto* c1 = new PHermiteCurveP2V2(
    PHermiteCurveP2V2::Point{4.0, 0.0, -4.0},
    PHermiteCurveP2V2::Point{4.0, 0.0, 4.0},
    PHermiteCurveP2V2::Vector{0.0, 80.0, 8.0},
    PHermiteCurveP2V2::Vector{0.0, 8.0, 8.0}
//    PHermiteCurveP2V2::Vector{0.0, 0.0, 8.0},
//    PHermiteCurveP2V2::Vector{0.0, 0.0, 8.0}
              );
    // clang-format on
    c1->initDefaultComponents();
    c1->defaultMesh()->setSamples(100);

    // clang-format off
  auto* g0 = new PHermiteCurveP2V2(
    PHermiteCurveP2V2::Point{-4.0, 0.0, -4.0},
    PHermiteCurveP2V2::Point{4.0, 0.0, -4.0},
    PHermiteCurveP2V2::Vector{8.0, 40.0, 0.0},
    PHermiteCurveP2V2::Vector{8.0, 0.0, 0.0});
    // clang-format on
    g0->initDefaultComponents();
    g0->defaultMesh()->setSamples(100);

    // clang-format off
  auto* g1 = new PHermiteCurveP2V2(
    PHermiteCurveP2V2::Point{-4.0, 0.0, 4.0},
    PHermiteCurveP2V2::Point{4.0, 0.0, 4.0},
    PHermiteCurveP2V2::Vector{8.0, 8.0, 0.0},
    PHermiteCurveP2V2::Vector{8.0, 8.0, 0.0});
    // clang-format on
    g1->initDefaultComponents();
    g1->defaultMesh()->setSamples(100);

    auto* c2 = new PCurve<detail::ZeroDerCurve>();
    auto* c3 = new PCurve<detail::ZeroDerCurve>();
    auto* g2 = new PCurve<detail::ZeroDerCurve>();
    auto* g3 = new PCurve<detail::ZeroDerCurve>();

    // Bicubic Coons patch
    auto* coons
      = new PBicubicCoonsPatch(c0, c1, c2, c3, g0, g1, g2, g3, scenegraph);
    c0->setParent(coons);
    c1->setParent(coons);
    c2->setParent(coons);
    c3->setParent(coons);
    g0->setParent(coons);
    g1->setParent(coons);
    g2->setParent(coons);
    g3->setParent(coons);
    coons->initDefaultComponents();
    coons->defaultMesh()->setSamples({50, 50});
  }

}   // namespace examples

#endif   // SCENES_OBJECTEXAMPLES_PCOONSPATCH_EXAMPLES_H

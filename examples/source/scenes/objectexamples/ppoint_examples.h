#ifndef SCENES_OBJECTEXAMPLES_PPOINT_EXAMPLES_H
#define SCENES_OBJECTEXAMPLES_PPOINT_EXAMPLES_H

// gmlib2
#include <gmlib2/qt/scenegraph.h>
#include <gmlib2/qt/exampleobjects/parametric/parametricobjects.h>

namespace examples
{

  // Declare contained examples
  void initParametricPointExample(gmlib2::qt::Scenegraph* scenegraph);


  // Examples
  void initParametricPointExample(gmlib2::qt::Scenegraph* scenegraph)
  {
    using namespace gmlib2::qt::parametric;

    // "a parametric point"
    auto* a_ppoint = new APPoint(scenegraph);
    a_ppoint->initDefaultComponents();
    a_ppoint->defaultMesh()->setRings(10);
    a_ppoint->defaultMesh()->setSlices(10);
  }

}   // namespace examples

#endif   // SCENES_OBJECTEXAMPLES_PPOINT_EXAMPLES_H

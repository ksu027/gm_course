#ifndef SCENES_OBJECTEXAMPLES_PPOLYGON_EXAMPLES_H
#define SCENES_OBJECTEXAMPLES_PPOLYGON_EXAMPLES_H

// gmlib2
#include <gmlib2/qt/scenegraph.h>
#include <gmlib2/qt/exampleobjects/parametric/parametricobjects.h>

namespace examples
{

  // Declare contained examples
  void initPPolygon4Example(gmlib2::qt::Scenegraph* scenegraph);
  void initPPolygon5Example(gmlib2::qt::Scenegraph* scenegraph);
  void initPPolygon10Example(gmlib2::qt::Scenegraph* scenegraph);


  // Examples
  void initPPolygon4Example(gmlib2::qt::Scenegraph* scenegraph)
  {
    using namespace gmlib2::qt::parametric;

    auto* poly4
      = new PPolygon4(PPolygon4::ControlPoints{PPolygon4::Point{0.0, 0.0, 0.0},
                                               PPolygon4::Point{4.0, 0.0, 0.0},
                                               PPolygon4::Point{4.0, 0.0, 4.0},
                                               PPolygon4::Point{0.0, 0.0, 4.0}},
                      scenegraph);
    poly4->initDefaultComponents();
    poly4->defaultMesh()->setSamples();
  }

  void initPPolygon5Example(gmlib2::qt::Scenegraph* scenegraph)
  {
    using namespace gmlib2::qt::parametric;

    auto* poly5 = new PPolygon5(
      PPolygon5::ControlPoints{
        PPolygon5::Point{-4.0, 0.0, -4.0}, PPolygon5::Point{0.0, 5.0, -4.0},
        PPolygon5::Point{4.0, 0.0, 0.0}, PPolygon5::Point{0.0, 0.0, 4.0},
        PPolygon5::Point{-4.0, 0.0, 0.0}},
      scenegraph);
    poly5->initDefaultComponents();
    poly5->defaultMesh()->setSamples();
  }

  void initPPolygon10Example(gmlib2::qt::Scenegraph* scenegraph)
  {
    namespace gm2p   = gmlib2::parametric;
    namespace gm2qt  = gmlib2::qt;
    namespace gm2qtp = gm2qt::parametric;

    using PPolygon10
      = gm2qtp::PPolygonSurface<gm2p::PPolygon<10, gm2qt::SceneObject>>;

    auto* poly10 = new PPolygon10(
      PPolygon10::ControlPoints{
        PPolygon10::Point{-1.5, 0.0, 0.25},    // p0
        PPolygon10::Point{-1.5, 0.0, -1.75},   // p1
        PPolygon10::Point{0, 0.0, -1.0},       // p2
        PPolygon10::Point{1.25, 0.0, -1.5},    // p3
        PPolygon10::Point{1.5, 0.0, -0.5},     // p4
        PPolygon10::Point{2.0, 0.0, 0.75},     // p5
        PPolygon10::Point{0.75, 0.0, 1.25},    // p6
        PPolygon10::Point{0, 0.0, 2.25},       // p7
        PPolygon10::Point{-1.0, 0.0, 1.5},     // p8
        PPolygon10::Point{-2.0, 0.0, 1.75}     // p9
      },
      scenegraph);
    poly10->initDefaultComponents();
    poly10->defaultMesh()->setSamples();
  }

}   // namespace examples

#endif   // SCENES_OBJECTEXAMPLES_PPOLYGON_EXAMPLES_H

import QtQuick 2.0
import QtQuick.Scene3D 2.0

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

import com.uit.GMlib2Qt 1.0

import "../../shared_assets/cameras"
import "../../shared_assets/framegraphs"
import "../../shared_assets/environments"

Item {
  id: single_camera_scene

  anchors.leftMargin: 10
  anchors.topMargin: 10
  anchors.rightMargin: 10
  anchors.bottomMargin: 10

  default property alias content: scene_root.childNodes
  property string scene_element_name: "scene_root_entity"
  property alias scenemodel: scene_root.scenemodel

  Scene3D {
    id: scene3d
    objectName: "scene3d"
    anchors.fill: parent
    focus: true
    aspects: ["input","logic"]
    cameraAspectRatioMode: Scene3D.AutomaticAspectRatio

    Entity {
      id: root_entity

      components: [ input_settings, render_settings]

      RenderSettings {
        id: render_settings
        activeFrameGraph: single_viewport_framegraph
        pickingSettings.pickMethod: PickingSettings.TrianglePicking
      }

      InputSettings { id: input_settings }


      SingleViewportFrameGraph {
        id: single_viewport_framegraph
        camera: main_camera
      }

      ProjectionCamera {
        id: main_camera
        position: Qt.vector3d(0.0, 20.0, 20.0)
        viewCenter: Qt.vector3d(0.0, 0.0, 0.0)
        upVector: Qt.vector3d(0.0, 1.0, 0.0)
      }

      OrbitCameraController{ camera: main_camera }

      SceneRootEntity {
        id: scene_root
        objectName: scene_element_name
      }

      DefaultSkyboxLightEnvironment {}

    } // END Entity (id:scene_root)

  } // END Scene3D


}

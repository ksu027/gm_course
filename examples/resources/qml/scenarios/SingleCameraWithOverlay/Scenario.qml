import QtQuick 2.0
import QtQuick.Scene3D 2.0

import Qt3D.Core 2.0
import Qt3D.Render 2.9
import Qt3D.Input 2.0
import Qt3D.Extras 2.9

import com.uit.GMlib2Qt 1.0

import "../../shared_assets/cameras"
import "../../shared_assets/environments"

Item {
  id: single_camera_scene

  anchors.leftMargin: 10
  anchors.topMargin: 10
  anchors.rightMargin: 10
  anchors.bottomMargin: 10

  default property alias content: scene_root.childNodes
  property string scene_element_name: "scene_root_entity"
  property alias scenemodel: scene_root.scenemodel

  Scene3D {
    id: scene3d
    objectName: "scene3d"
    anchors.fill: parent
    focus: true
    aspects: ["input","logic"]
    cameraAspectRatioMode: Scene3D.AutomaticAspectRatio

    Entity {
      id: root_entity

      components: [ input_settings, render_settings]

      RenderSettings {
        id: render_settings
        activeFrameGraph: FrameGraph {
          id: fg
          select_layer: scene_root.select_layer
        }
        pickingSettings.pickMethod: PickingSettings.TrianglePicking
        pickingSettings.faceOrientationPickingMode: PickingSettings.FrontAndBackFace
      }

      InputSettings { id: input_settings }

      ProjectionCamera {
        id: projection_camera
      }

      OrbitCameraController{ camera: projection_camera }

      Entity {
        id: content_layer_entity

        SceneRootEntity {
          id: scene_root
          objectName: scene_element_name
        }

        DefaultSkyboxLightEnvironment {}

        components: [fg.scene_layer]
      }

      Entity {
        id: overlay_layer_entity

        OverlayScene {
          id: overlay_scene
          cubeViewMatrix: projection_camera.viewMatrix
        }

        components: [fg.overlay_layer]
      }

    } // END Entity (id:scene_root)


  } // END Scene3D

}

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Input 2.0
import Qt3D.Animation 2.10
import Qt3D.Extras 2.10

import com.uit.GMlib2Qt 1.0

import "qrc:///qt3d_examples/animated_skinned_mesh" as ASM

Entity {
  property alias transform: trans

  Transform {
    id: trans
  }

  components: [trans]

  AnimationClipLoader {
    id: animationClip
    source: "qrc:///ext_shared_assets/qt3d/models/robot/robot-talk.gltf"
    onSourceChanged: {
      riggedFigure1.running = false
      riggedFigure1.start()
    }
  }

  ASM.SkinnedPbrEffect {
    id: texturedSkinnedPbrEffect
    useTextures: true
  }

  ASM.AnimatedEntity {
    id: riggedFigure1

    source: "qrc:///ext_shared_assets/qt3d/models/robot/robot.gltf"
    clip: animationClip
    playbackRate: 1.0

    transform.scale: 0.035

    effect: texturedSkinnedPbrEffect
    textureBaseName: "qrc:///ext_shared_assets/qt3d/models/robot/robot"
  }

}

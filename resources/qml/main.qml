import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

import Qt3D.Core 2.0
import Qt3D.Input 2.0
import Qt3D.Animation 2.10

import "scenes" as Sce

ApplicationWindow {
  id: root
  visible: true
  width: 800
  height: 600

  menuBar: MenuBar {
    Menu {
      title: "Scenes"
      Menu {
        title: "Sliding";
        MenuItem {
            text: "90\u02DA angle";
            onTriggered: scene_loader.newScene("qrc:/qml/scenes/Ninesliding.qml")
        }
        MenuItem {
            text: "Sharp angle";
            onTriggered: scene_loader.newScene("qrc:/qml/scenes/Sharpsliding.qml")
        }
        MenuItem {
            text: "Wide angle";
            onTriggered: scene_loader.newScene("qrc:/qml/scenes/Widesliding.qml")
        }
        MenuItem {
            text: "Wide continue";
            onTriggered: scene_loader.newScene("qrc:/qml/scenes/Continuesliding.qml")
        }
        MenuItem {
          text: "Slide to rest";
          onTriggered: scene_loader.newScene("qrc:/qml/scenes/InnerSpheres.qml")
        }
      }
      Menu {
        title: "Multi Spheres";
        MenuItem {
          text: "Newton’s cradle";
          onTriggered: scene_loader.newScene("qrc:/qml/scenes/Momentum.qml")
        }
        MenuItem {
          text: "Triangle";
          onTriggered: scene_loader.newScene("qrc:/qml/scenes/Triangle.qml")
        }
        MenuItem {
          text: "Collision+slide/rest";
          onTriggered: scene_loader.newScene("qrc:/qml/scenes/StateChange.qml")
        }
        MenuItem {
          text: "Collision+slide/rest+angle";
          onTriggered: scene_loader.newScene("qrc:/qml/scenes/StateChange1.qml")
        }
        MenuItem {
          text: "Collision+rest+90\u02DA";
          onTriggered: scene_loader.newScene("qrc:/qml/scenes/StateChange2.qml")
        }
        MenuItem {
          text: "Sphere slide on sphere";
          onTriggered: scene_loader.newScene("qrc:/qml/scenes/StateChange_one.qml")
        }
        MenuItem {
            text: "Rest/Slide sphere on sphere";
            onTriggered: scene_loader.newScene("qrc:/qml/scenes/Multirest.qml")
        }
        MenuItem {
          text: "Sphere rest";
          onTriggered: scene_loader.newScene("qrc:/qml/scenes/SphereRest.qml")
        }
      }
      Menu{
        title: "Inner Loop";
        MenuItem {
          text: "Sphere Plane";
          onTriggered: scene_loader.newScene("qrc:/qml/scenes/Innerloop.qml")
        }
        MenuItem {
          text: "Sphere Sphere";
          onTriggered: scene_loader.newScene("qrc:/qml/scenes/InnerloopSP.qml")
        }
      }

      MenuSeparator{}
      MenuItem { text: "Quit"; onTriggered: Qt.quit() }
    }
  }

  statusBar: Item {
    anchors.bottom: parent.bottom
    anchors.left: parent.left
    anchors.right: parent.right
    height: 40
    RowLayout {
      anchors.fill: parent
      anchors.leftMargin: 5
      anchors.rightMargin: 5
      Item{ Layout.fillWidth: true }
      CheckBox{ text: "e-mode"; checked: scenario.eMode ; onCheckedChanged: scenario.eMode=!scenario.eMode }
      Button{ text: "start/stop";  onClicked: scenario.simulatorRunStatus = !scenario.simulatorRunStatus }
      Button{ text: "reload"; onClicked: scene_loader.reload() }
    }
  }

  Scenario {

    id: scenario
    anchors.fill: parent
    EntityLoader {
        id: scene_loader
        //source: "qrc:/qml/scenes/Innerloop.qml"
        source:""
        onStatusChanged: {
            if(status !== EntityLoader.Ready) return
        }
        function reload() {
            newScene(source)
        }
        function newScene(new_sorce) {
            source = ""
            source = new_sorce
        }
    }
  }
}

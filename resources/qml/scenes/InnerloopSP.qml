import QtQuick 2.2

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Extras 2.10

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

import "../objects" as Obj

SceneObject {

  RB.Environment {
    id: rba_environment
    gravity:  Qt.vector3d(0.0,0.0,0.0)
  }

  RB.Environment {
    id: rba_environment2
    gravity:  Qt.vector3d(0.0,-10.0,0.0)
  }

  property alias radius: ball.radius


  Ground {
    id: top2
    defaultMesh.samples: Qt.size(2,2)
    RB.PlaneController{
      id: top2_rbc
      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.StaticObject
      p: top2.pt
      u: top2.u
      v: top2.v
    }

    Component.onCompleted: {
      // place object
      setParametersQt(Qt.vector3d(-10,3.1,-10),Qt.vector3d(0,0,20),Qt.vector3d(20,0,0))
      // reset plane RB-controller
      top2_rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

  Ground {
    id: ground
    defaultMesh.samples: Qt.size(2,2)
    RB.PlaneController{
      id: ground_rbc
      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.StaticObject
      p: ground.pt
      u: ground.u
      v: ground.v
    }
    Component.onCompleted: {
      // place object
      setParametersQt(Qt.vector3d(-10,3,-10),Qt.vector3d(20,0,0),Qt.vector3d(0,0,20))
      // reset plane RB-controller
      ground_rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

  Ground {
    id: side_1
    defaultMesh.samples: Qt.size(2,2)
    RB.PlaneController{
      id: side_1_rbc
      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.StaticObject
      p: side_1.pt
      u: side_1.u
      v: side_1.v
    }

    Component.onCompleted: {
      // place object
      setParametersQt(Qt.vector3d(-10,-1,-10),Qt.vector3d(0,0,20),Qt.vector3d(20,0,0))
      // reset plane RB-controller
      side_1_rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

  Ground {
    id: side_2
    defaultMesh.samples: Qt.size(2,2)
    RB.PlaneController{
      id: side_2_rbc
      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.StaticObject
      p: side_2.pt
      u: side_2.u
      v: side_2.v
    }
    Component.onCompleted: {
      // place object
      setParametersQt(Qt.vector3d(-10,-1.2,-10),Qt.vector3d(0,0,20),Qt.vector3d(20,0,0))
      // reset plane RB-controller
      side_2_rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

  Ball {
    id: ball
    radius: 0.5
    defaultMesh.samples: Qt.size(20,20)
    onRadiusChanged: defaultMesh.reSample()
    defaultMaterial.diffuse: "white"
    RB.SphereController{
      id: ball_rbc
      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.DynamicObject
      radius: ball.radius
      velocity: Qt.vector3d(0.0,-200.0,0.0)
      mass: 1.0
      gravity: rba_environment.gravity
      friction: 1.0
      onFrameComputed: ball.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {
      // initial object placement
      translateGlobalQt( Qt.vector3d(0,2.0,0) )
      // reset sphere RB-controller
      ball_rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

  Ball {
    id: ball2

    radius: 0.5

    defaultMesh.samples: Qt.size(20,20)

    onRadiusChanged: defaultMesh.reSample()

    RB.SphereController{
      id: ball2_rbc

      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.DynamicObject

      radius: ball2.radius
      velocity: Qt.vector3d(0.0,200.0,0.0)
      mass: 1.0
      gravity: rba_environment.gravity
      friction: 1.0
      onFrameComputed: ball2.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {

      // initial object placement
      translateGlobalQt( Qt.vector3d(0,0,0) )

      // reset sphere RB-controller
      ball2_rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

  Ball {
    id: ball3

    radius: 0.5

    defaultMesh.samples: Qt.size(20,20)

    onRadiusChanged: defaultMesh.reSample()

    RB.SphereController{
      id: ball3_rbc

      environment: rba_environment2
      dynamicsType: RB.RigidBodyNS.DynamicObject

      radius: ball3.radius
      velocity: Qt.vector3d(0.0,0.0,0.0)
      mass: 1.0
      gravity: rba_environment2.gravity
      friction: 0.9
      onFrameComputed: ball3.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {

      // initial object placement
      translateGlobalQt( Qt.vector3d(0,4,0) )

      // reset sphere RB-controller
      ball3_rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }
}

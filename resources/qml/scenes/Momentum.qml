import QtQuick 2.2

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Extras 2.10

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

import "../objects" as Obj

SceneObject {

  RB.Environment {
    id: rba_environment
    gravity: Qt.vector3d(0,0,0)
  }
  property alias radius: ball.radius

  Ball {
    id: ball

    radius: 0.5

    defaultMesh.samples: Qt.size(20,20)

    onRadiusChanged: defaultMesh.reSample()

    RB.SphereController{
      id: ball_rbc

      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.DynamicObject

      radius: ball.radius
      velocity: Qt.vector3d(5.0,0.0,0)
      mass: 1.0
      gravity: rba_environment.gravity
      friction: 1.0

      onFrameComputed: ball.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {

      // initial object placement
      translateGlobalQt( Qt.vector3d(-3.49,0.0,0) )

      // reset sphere RB-controller
      ball_rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

  Ball {
    id: ball2

    radius: 0.5

    defaultMesh.samples: Qt.size(20,20)

    onRadiusChanged: defaultMesh.reSample()

    RB.SphereController{
      id: ball_rbc2

      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.DynamicObject

      radius: ball2.radius
      velocity: Qt.vector3d(0,0,0)
      mass: 1.0
      gravity: rba_environment.gravity
      friction: 1.0

      onFrameComputed: ball2.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {

      // initial object placement
      translateGlobalQt( Qt.vector3d(-1.5975,0.0,0) )

      // reset sphere RB-controller
      ball_rbc2.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

  Ball {
    id: ball3

    radius: 0.5

    defaultMesh.samples: Qt.size(20,20)

    onRadiusChanged: defaultMesh.reSample()

    RB.SphereController{
      id: ball_rbc3

      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.DynamicObject

      radius: ball3.radius
      velocity: Qt.vector3d(0,0,0)
      mass: 1.0
      gravity: rba_environment.gravity
      friction: 1.0

      onFrameComputed: ball3.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {

      // initial object placement
      translateGlobalQt( Qt.vector3d(-0.5475,0.0,0) )

      // reset sphere RB-controller
      ball_rbc3.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

  Ball {
    id: ball4

    radius: 0.5

    defaultMesh.samples: Qt.size(20,20)

    onRadiusChanged: defaultMesh.reSample()

    RB.SphereController{
      id: ball_rbc4

      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.DynamicObject

      radius: ball4.radius
      velocity: Qt.vector3d(0,0,0)
      mass: 1.0
      gravity: rba_environment.gravity
      friction: 1.0

      onFrameComputed: ball4.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {

      // initial object placement
      translateGlobalQt( Qt.vector3d(0.5025,0.0,0) )

      // reset sphere RB-controller
      ball_rbc4.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

  Ball {
    id: ball5

    radius: 0.5

    defaultMesh.samples: Qt.size(20,20)

    onRadiusChanged: defaultMesh.reSample()

    RB.SphereController{
      id: ball_rbc5

      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.DynamicObject

      radius: ball5.radius
      velocity: Qt.vector3d(0,0,0)
      mass: 1.0
      gravity: rba_environment.gravity
      friction: 1.0

      onFrameComputed: ball5.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {

      // initial object placement
      translateGlobalQt( Qt.vector3d(1.5525,0.0,0) )

      // reset sphere RB-controller
      ball_rbc5.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }


  Ground {
    id: ground_2

    //defaultMesh.samples: Qt.size(2,2)

    RB.PlaneController{
      id: ground_rbc_2

      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.StaticObject

      p: ground_2.pt
      u: ground_2.u
      v: ground_2.v
    }

    Component.onCompleted: {

      // place object
      setParametersQt(Qt.vector3d(3,5,-10),Qt.vector3d(0,-20,0),Qt.vector3d(0,0,20))

      // reset plane RB-controller
      ground_rbc_2.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

  Ground {
    id: ground_1

    //defaultMesh.samples: Qt.size(2,2)

    RB.PlaneController{
      id: ground_rbc_1

      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.StaticObject

      p: ground_1.pt
      u: ground_1.u
      v: ground_1.v
    }

    Component.onCompleted: {

      // place object
      setParametersQt(Qt.vector3d(-4,5,-10),Qt.vector3d(0,0,20),Qt.vector3d(0,-20,0))

      // reset plane RB-controller
      ground_rbc_1.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

}

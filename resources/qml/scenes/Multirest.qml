import QtQuick 2.2

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Extras 2.10

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

import "../objects" as Obj

SceneObject {

  RB.Environment {
    id: rba_environment
    gravity: Qt.vector3d(0,0,0)
  }

  RB.Environment {
    id: rba_environment2
    gravity: Qt.vector3d(0,-10,0)
  }

  property alias radius: ball.radius



  Ball {
    id: ball

    radius: 0.5

    defaultMesh.samples: Qt.size(20,20)

    onRadiusChanged: defaultMesh.reSample()

    RB.SphereController{
      id: ball_rbc

      environment: rba_environment2
      dynamicsType: RB.RigidBodyNS.DynamicObject

      radius: ball.radius
      velocity: Qt.vector3d(0.0,0,0)
      mass: 1.0
      gravity: rba_environment2.gravity

      onFrameComputed: ball.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {

      // initial object placement
      translateGlobalQt( Qt.vector3d(0.0,3.0,0) )

      // reset sphere RB-controller
      ball_rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

  Ball {
    id: ball2

    radius: 2.0

    defaultMesh.samples: Qt.size(20,20)

    onRadiusChanged: defaultMesh.reSample()

    RB.SphereController{
      id: ball_rbc2

      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.DynamicObject

      radius: ball2.radius
      velocity: Qt.vector3d(0,0,0)
      mass: 500.0
      gravity: rba_environment.gravity

      onFrameComputed: ball2.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {

      // initial object placement
      translateGlobalQt( Qt.vector3d(0,0,0) )

      // reset sphere RB-controller
      ball_rbc2.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }


  Ball {
    id: ball3

    radius: 0.5

    defaultMesh.samples: Qt.size(20,20)

    onRadiusChanged: defaultMesh.reSample()

    RB.SphereController{
      id: ball_rbc3

      environment: rba_environment2
      dynamicsType: RB.RigidBodyNS.DynamicObject

      radius: ball3.radius
      velocity: Qt.vector3d(0.0,0,0)
      mass: 1.0
      gravity: rba_environment2.gravity

      onFrameComputed: ball3.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {

      // initial object placement
      translateGlobalQt( Qt.vector3d(5.05,3.0,0) )

      // reset sphere RB-controller
      ball_rbc3.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

  Ball {
    id: ball4

    radius: 2.0

    defaultMesh.samples: Qt.size(20,20)

    onRadiusChanged: defaultMesh.reSample()

    RB.SphereController{
      id: ball_rbc4

      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.DynamicObject

      radius: ball4.radius
      velocity: Qt.vector3d(0,0,0)
      mass: 500.0
      gravity: rba_environment.gravity

      onFrameComputed: ball4.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {

      // initial object placement
      translateGlobalQt( Qt.vector3d(5,0,0) )

      // reset sphere RB-controller
      ball_rbc4.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }
}

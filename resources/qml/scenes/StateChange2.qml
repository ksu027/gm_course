import QtQuick 2.2

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Extras 2.10

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

import "../objects" as Obj

SceneObject {

  RB.Environment {
    id: rba_environment
    gravity: Qt.vector3d(0,0,0)
  }

  RB.Environment {
    id: rba_environment2
    gravity: Qt.vector3d(0,-10,0)
  }

  property alias radius: ball.radius



  Ground {
    id: side_1

    defaultMesh.samples: Qt.size(2,2)

    RB.PlaneController{
      id: side_1_rbc

      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.StaticObject

      p: side_1.pt
      u: side_1.u
      v: side_1.v
    }

    Component.onCompleted: {

      // place object
      setParametersQt(Qt.vector3d(-10,0,-10),Qt.vector3d(0,0,20),Qt.vector3d(20,0,0))

      // reset plane RB-controller
      side_1_rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

  Ball {
    id: ball

    radius: 0.5

    defaultMesh.samples: Qt.size(20,20)

    onRadiusChanged: defaultMesh.reSample()

    RB.SphereController{
      id: ball_rbc

      environment: rba_environment2
      dynamicsType: RB.RigidBodyNS.DynamicObject

      radius: ball.radius
      velocity: Qt.vector3d(0.0,0.0,0)
      mass: 2.0
      gravity: rba_environment2.gravity

      onFrameComputed: ball.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {

      // initial object placement
      translateGlobalQt( Qt.vector3d(0,0.501,0) )

      // reset sphere RB-controller
      ball_rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }



  Ball {
    id: ball3

    radius: 0.5

    defaultMesh.samples: Qt.size(20,20)

    onRadiusChanged: defaultMesh.reSample()
    defaultMaterial.diffuse: "white"

    RB.SphereController{
      id: ball_rbc3

      environment: rba_environment2
      dynamicsType: RB.RigidBodyNS.DynamicObject

      radius: ball3.radius
      velocity: Qt.vector3d(0,0,0)
      mass: 1.0
      gravity: rba_environment2.gravity

      onFrameComputed: ball3.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {

      // initial object placement
      translateGlobalQt( Qt.vector3d(0.0,4.0,0) )

      // reset sphere RB-controller
      ball_rbc3.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }


  Ball {
    id: ball4

    radius: 2.0

    defaultMesh.samples: Qt.size(20,20)

    onRadiusChanged: defaultMesh.reSample()

    RB.SphereController{
      id: ball_rbc4

      environment: rba_environment2
      dynamicsType: RB.RigidBodyNS.DynamicObject

      radius: ball4.radius
      velocity: Qt.vector3d(0.0,0.0,0)
      mass: 500.0
      gravity: rba_environment2.gravity

      onFrameComputed: ball4.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {

      // initial object placement
      translateGlobalQt( Qt.vector3d(4,2.01,0) )

      // reset sphere RB-controller
      ball_rbc4.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

  Ball {
    id: ball5

    radius: 0.5

    defaultMesh.samples: Qt.size(20,20)

    onRadiusChanged: defaultMesh.reSample()
    defaultMaterial.diffuse: "white"

    RB.SphereController{
      id: ball_rbc5

      environment: rba_environment2
      dynamicsType: RB.RigidBodyNS.DynamicObject

      radius: ball5.radius
      velocity: Qt.vector3d(0,0,0)
      mass: 1.0
      gravity: rba_environment2.gravity

      onFrameComputed: ball5.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {

      // initial object placement
      translateGlobalQt( Qt.vector3d(4.01,6.0,0) )

      // reset sphere RB-controller
      ball_rbc5.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }


  Ball {
    id: ball6

    radius: 2.0

    defaultMesh.samples: Qt.size(20,20)

    onRadiusChanged: defaultMesh.reSample()

    RB.SphereController{
      id: ball_rbc6

      environment: rba_environment2
      dynamicsType: RB.RigidBodyNS.DynamicObject

      radius: ball6.radius
      velocity: Qt.vector3d(0.0,0.0,0)
      mass: 100.0
      gravity: rba_environment2.gravity

      onFrameComputed: ball6.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {

      // initial object placement
      translateGlobalQt( Qt.vector3d(-4,2.01,0) )

      // reset sphere RB-controller
      ball_rbc6.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

  Ball {
    id: ball7

    radius: 0.5

    defaultMesh.samples: Qt.size(20,20)

    onRadiusChanged: defaultMesh.reSample()
    defaultMaterial.diffuse: "white"

    RB.SphereController{
      id: ball_rbc7

      environment: rba_environment2
      dynamicsType: RB.RigidBodyNS.DynamicObject

      radius: ball7.radius
      velocity: Qt.vector3d(0,0,0)
      mass: 1.0
      gravity: rba_environment2.gravity

      onFrameComputed: ball7.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {

      // initial object placement
      translateGlobalQt( Qt.vector3d(-4.0,6.0,0) )

      // reset sphere RB-controller
      ball_rbc7.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

}

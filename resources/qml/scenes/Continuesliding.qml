import QtQuick 2.2

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Extras 2.10

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

import "../objects" as Obj

SceneObject {

  RB.Environment {
    id: rba_environment
    gravity: Qt.vector3d(0.0,-10.0,0.0)
  }

  property alias radius: ball.radius



  Ground {
    id: side_1

    defaultMesh.samples: Qt.size(2,2)

    RB.PlaneController{
      id: side_1_rbc

      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.StaticObject

      p: side_1.pt
      u: side_1.u
      v: side_1.v
    }

    Component.onCompleted: {

      // place object
      setParametersQt(Qt.vector3d(-10,-1,-10),Qt.vector3d(0,-5,20),Qt.vector3d(20,0,0))

      // reset plane RB-controller
      side_1_rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

//continue slide
  Ground {
    id: side_2

    defaultMesh.samples: Qt.size(2,2)

    RB.PlaneController{
      id: side_2_rbc

      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.StaticObject

      p: side_2.pt
      u: side_2.u
      v: side_2.v
    }

    Component.onCompleted: {

      // place object
      setParametersQt(Qt.vector3d(-10,-3,-10),Qt.vector3d(0,-2,20),Qt.vector3d(20,0,0))

      // reset plane RB-controller
      side_2_rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

//stop slide wide
//  Ground {
//    id: side_2

//    defaultMesh.samples: Qt.size(2,2)

//    RB.PlaneController{
//      id: side_2_rbc

//      environment: rba_environment
//      dynamicsType: RB.RigidBodyNS.StaticObject

//      p: side_2.pt
//      u: side_2.u
//      v: side_2.v
//    }

//    Component.onCompleted: {

//      // place object
//      setParametersQt(Qt.vector3d(-10,-15,-10),Qt.vector3d(0,20,20),Qt.vector3d(20,0,0))

//      // reset plane RB-controller
//      side_2_rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
//    }
//  }




//  Ground {
//    id: side_2

//    defaultMesh.samples: Qt.size(2,2)

//    RB.PlaneController{
//      id: side_2_rbc

//      environment: rba_environment
//      dynamicsType: RB.RigidBodyNS.StaticObject

//      p: side_2.pt
//      u: side_2.u
//      v: side_2.v
//    }

//    Component.onCompleted: {

//      // place object
//      setParametersQt(Qt.vector3d(-10,-10,-10),Qt.vector3d(20,20,0),Qt.vector3d(0,0,20))

//      // reset plane RB-controller
//      side_2_rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
//    }
//  }

  Ball {
    id: ball

    radius: 0.5

    defaultMesh.samples: Qt.size(20,20)

    onRadiusChanged: defaultMesh.reSample()

    RB.SphereController{
      id: ball_rbc

      environment: rba_environment
      dynamicsType: RB.RigidBodyNS.DynamicObject

      radius: ball.radius

      velocity: Qt.vector3d(0,0,0)
      mass: 1.0
      gravity: rba_environment.gravity


      onFrameComputed: ball.setFrameParentQt(dir,up,pos);
    }

    Component.onCompleted: {

      // initial object placement
//      translateGlobalQt( Qt.vector3d(0,0,-9.7) )
      translateGlobalQt( Qt.vector3d(0,-0.5,-9.7) )

      // reset sphere RB-controller
      ball_rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
  }

//  Ball {
//    id: ball2

//    radius: 0.5

//    defaultMesh.samples: Qt.size(20,20)

//    onRadiusChanged: defaultMesh.reSample()

//    RB.SphereController{
//      id: ball_rbc2

//      environment: rba_environment
//      dynamicsType: RB.RigidBodyNS.DynamicObject

//      radius: ball2.radius

//      onFrameComputed: ball2.setFrameParentQt(dir,up,pos);
//    }

//    Component.onCompleted: {

//      // initial object placement
//      translateGlobalQt( Qt.vector3d(3,6,0) )

//      // reset sphere RB-controller
//      ball_rbc2.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
//    }
//  }

}

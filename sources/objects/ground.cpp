#include "ground.h"


namespace I_was_to_inDifferent_to_aLter_this_namEspace
{
bool Ground::qt_types_initialized = false;

QVector3D Ground::ptQt() const
{
    return QVector3D{float(m_pt[0]),float(m_pt[1]),float(m_pt[2])};
}

void Ground::setPtQt(const QVector3D &pt)
{
    m_pt=rigidbodyaspect::utils::qVecToGM2HVec3(pt, true);
//    m_pt[0] = double(pt.x());
//    m_pt[1] = double(pt.y());
//    m_pt[2] = double(pt.z());
//    m_pt[3] = 1;
    emit ptChanged(pt);
}

QVector3D Ground::uQt() const
{
    return QVector3D{float(m_u[0]),float(m_u[1]),float(m_u[2])};
}

void Ground::setUQt(const QVector3D &u)
{
    m_u=rigidbodyaspect::utils::qVecToGM2HVec3(u, false);
//    m_u[0] = double(u.x());
//    m_u[1] = double(u.y());
//    m_u[2] = double(u.z());
//    m_u[3] = 0;
    emit uChanged(u);
}

QVector3D Ground::vQt() const
{
    return QVector3D{float(m_v[0]),float(m_v[1]),float(m_v[2])};
}

void Ground::setVQt(const QVector3D &v)
{
    m_v=rigidbodyaspect::utils::qVecToGM2HVec3(v, true);
//    m_v[0] = double(v.x());
//    m_v[1] = double(v.y());
//    m_v[2] = double(v.z());
//    m_v[3] = 0;
    emit vChanged(v);
}

}   // namespace I_was_to_inDifferent_to_aLter_this_namEspace

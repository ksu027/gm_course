#ifndef GROUND_H
#define GROUND_H

#include <gmlib2/qt/exampleobjects/parametric/psurface.h>

#include "rigidbodyaspect/utils.h"

// qt
#include <QQmlEngine>


namespace I_was_to_inDifferent_to_aLter_this_namEspace
{

  class Ground : public gmlib2::qt::parametric::PSurface<
                   gmlib2::parametric::PPlane<gmlib2::qt::SceneObject>> {
    using Base = PSurface<gmlib2::parametric::PPlane<gmlib2::qt::SceneObject>>;
    Q_OBJECT

    Q_PROPERTY(
      gmlib2::qt::parametric::PSurfaceMesh* defaultMesh READ defaultMesh)
    Q_PROPERTY(QVector3D pt READ ptQt WRITE setPtQt NOTIFY ptChanged)
    Q_PROPERTY(QVector3D u READ uQt WRITE setUQt NOTIFY uChanged)
    Q_PROPERTY(QVector3D v READ vQt WRITE setVQt NOTIFY vChanged)

  public:

    // Constructor(s)
    template <typename... Ts>
    Ground(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
      initDefaultComponents();
    }

    Q_INVOKABLE void setParametersQt(const QVector3D& pt, const QVector3D& u,
                                     const QVector3D& v)
    {

      m_pt = VectorH_Type{double(pt.x()), double(pt.y()), double(pt.z()), 1.0};
      m_u  = VectorH_Type{double(u.x()), double(u.y()), double(u.z()), 0.0};
      m_v  = VectorH_Type{double(v.x()), double(v.y()), double(v.z()), 0.0};

      if (defaultMesh()) defaultMesh()->reSample();

      emit ptChanged(pt);
      emit uChanged(u);
      emit vChanged(v);

    }

    static void registerQmlTypes(int version_major, int version_minor)
    {
      if(qt_types_initialized) return;

      constexpr auto registertype_uri = "com.uit.STE6245";
//      qDebug() << "Trying to register type <Ground>; getting id: " <<
      qmlRegisterType<Ground>(registertype_uri, version_major, version_minor,
                              "Ground");

      qt_types_initialized = true;
    }

    QVector3D ptQt() const;
    void setPtQt( const QVector3D& pt);

    QVector3D uQt() const;
    void setUQt( const QVector3D& u);

    QVector3D vQt() const;
    void setVQt( const QVector3D& v);

    // Signal(s)
  signals:
    void ptChanged(QVector3D);
    void uChanged(QVector3D);
    void vChanged(QVector3D);

  private:
    static bool qt_types_initialized;
  };

}   // namespace I_was_to_inDifferent_to_aLter_this_namEspace


#endif   // GROUND_H

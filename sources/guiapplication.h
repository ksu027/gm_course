#ifndef GUIAPPLICATION_H
#define GUIAPPLICATION_H


// gmlib2
#include <gmlib2/qt/scenegraph.h>

// qt
#include <QGuiApplication>
#include <QQmlApplicationEngine>

// stl
#include <memory>

class GuiApplication : public QGuiApplication {
public:
  GuiApplication(int& argc, char** argv);

  // Entry point
  void initializeScenario();

private:
  QQmlApplicationEngine   m_qml_engine;
  gmlib2::qt::Scenegraph* m_scenegraph{nullptr};
};

#endif   // GUIAPPLICATION_H

#include "guiapplication.h"

#include "objects/ball.h"
#include "objects/ground.h"

// Rigid Body Aspect
#include <rigidbodyaspect/init.h>

// gmlib2
#include <gmlib2/qt/integration.h>

// qt
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <Qt3DCore>
#include <Qt3DExtras>
#include <QQmlContext>

// stl
#include <memory>
#include <chrono>
using namespace std::chrono_literals;



int main(int argc, char** argv) try {

  // Force "high" OpenGL format * Mac OSX and Linux Intel
  auto format = QSurfaceFormat::defaultFormat();
  format.setProfile(QSurfaceFormat::OpenGLContextProfile::CoreProfile);
  format.setMajorVersion(4);
  format.setMinorVersion(1);
  format.setDepthBufferSize(24);
  format.setStencilBufferSize(8);
  format.setSamples(4);
  QSurfaceFormat::setDefaultFormat(format);

  // Register custom QML types
  gmlib2::qt::init::registerQmlTypes();
  gmlib2::qt::init::registerExampleObjectsAsQmlTypes();

  // RigidBody Aspect
  rigidbodyaspect::init::registerQmlTypes();

  // register my types
  {

    namespace ns = I_was_to_inDifferent_to_aLter_this_namEspace;

    // Register Object QML types
    ns::Ball::registerQmlTypes(1,0);
    ns::Ground::registerQmlTypes(1,0);
  }

  // Start application
  GuiApplication app(argc, argv);
  app.initializeScenario();
  return app.exec();
}
catch (std::runtime_error exception) {
  std::cerr << "Runtime exception: " << exception.what() << std::endl;
}

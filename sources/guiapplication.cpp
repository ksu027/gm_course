#include "guiapplication.h"

// examples
#include "../examples/source/examples.h"

// gmlib2
#include <gmlib2/qt/scenegraph.h>
#include <gmlib2/qt/exampleobjects/parametric/parametricobjects.h>

// qt
#include <QQmlContext>
#include <Qt3DExtras>

// stl
#include <exception>


// Details ^^,
namespace detail
{
  void drawObjectTree(const QObjectList& objects, const QString& indent)
  {
    for (auto* object : objects) {
      qDebug() << indent + "ObjectName: " + object->objectName();
      drawObjectTree(object->children(), indent + " ");
    }
  }
}   // namespace detail


GuiApplication::GuiApplication(int& argc, char** argv)
  : QGuiApplication(argc, argv)
{

  const auto resource_file_status = QResource::registerResource("external_shared_assets_qt3d.rcc");
  qDebug() << "Resource file status: " << resource_file_status;

  m_qml_engine.load(QUrl("qrc:///qml/main.qml"));

  auto root_objects = m_qml_engine.rootObjects();

  // Runtime checks
  if (root_objects.isEmpty()) throw std::runtime_error("No QML root object");

  if (root_objects.size() not_eq 1)
    throw std::runtime_error("To many root object");

  // Fetch the root object
  auto* root_object = root_objects.at(0);

  auto  scene_root_entity_name = QString("scene_root_entity");
  auto* scene_root = root_object->findChild<gmlib2::qt::Scenegraph*>(
    scene_root_entity_name, Qt::FindChildrenRecursively);
  if (not scene_root)
    throw std::runtime_error("No <" + scene_root_entity_name.toStdString()
                             + "> root object defined!!");

  m_scenegraph = scene_root;
}

void GuiApplication::initializeScenario()
{

  if (not m_scenegraph) return;
}

#ifndef RIGIDBODYASPECT_CONSTANTS_H
#define RIGIDBODYASPECT_CONSTANTS_H

#include "types.h"

namespace rigidbodyaspect::constants {

  constexpr auto Gn = GM2UnitType{980.665};  // [cm/s^2]
  const auto eps_double = 0.0000001;
  const auto eps_float = 0.00001;

}   // namespace rigidbodyaspect



#endif // RIGIDBODYASPECT_CONSTANTS_H

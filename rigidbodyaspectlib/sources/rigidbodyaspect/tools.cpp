#include "tools.h"

namespace rigidbodyaspect::tools
{
  planeProps getPlaneProps(const rbtypes::FixedPlane& plane)
  {
      const auto pl_eval = plane.evaluateParent(rbtypes::FixedPlane::PSpacePoint{0.0,0.0},
                                                rbtypes::FixedPlane::PSizeArray{1UL,1UL});
      const auto pl_u = blaze::subvector<0UL,3UL>(pl_eval(1UL,0UL));
      const auto pl_v = blaze::subvector<0UL,3UL>(pl_eval(0UL,1UL));

      const auto q = blaze::subvector<0UL,3UL>(pl_eval(0UL,0UL));
      const auto n = blaze::normalize(blaze::cross(pl_u,pl_v));
      return std::make_tuple(pl_u,pl_v,q,n);
  };
};

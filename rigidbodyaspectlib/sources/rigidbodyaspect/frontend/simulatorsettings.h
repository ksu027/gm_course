#ifndef RIGIDBODYASPECT_SIMULATORSETTINGS_H
#define RIGIDBODYASPECT_SIMULATORSETTINGS_H

#include "../types.h"

// blaze
#include <blaze/Math.h>

// qt
#include <QMatrix4x4>
#include <Qt3DCore/QComponent>
#include <Qt3DCore/QTransform>

namespace rigidbodyaspect
{

  class SimulatorSettings : public Qt3DCore::QComponent {
    Q_OBJECT

    Q_PROPERTY(
      bool runStatus READ runStatus WRITE setRunStatus NOTIFY runStatusChanged)

    Q_PROPERTY(bool eMode READ eMode WRITE seteMode NOTIFY eModeChanged)

  public:
    SimulatorSettings(Qt3DCore::QNode* parent = nullptr);
    ~SimulatorSettings() override = default;

    bool runStatus() const;
    void setRunStatus(bool run_status);

    bool eMode() const;
    void seteMode(bool mode);

  private:

    bool m_run_status;
    bool m_emode;

  signals:
    void runStatusChanged(bool status);
    void eModeChanged(bool mode);

    // QNode interface
  private:
    Qt3DCore::QNodeCreatedChangeBasePtr createNodeCreationChange() const override;
  };

  struct SimulatorSettingsInitialData {
    bool m_run_status;
    bool m_emode;
  };

}   // namespace rigidbodyaspect




#endif // RIGIDBODYASPECT_SIMULATORSETTINGS_H

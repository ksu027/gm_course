#include "simulatorsettings.h"

#include <QDebug>

namespace rigidbodyaspect
{

  SimulatorSettings::SimulatorSettings(Qt3DCore::QNode* parent)
    : Qt3DCore::QComponent(parent)
  {
    qDebug() << "Constructing SimulatorSettings frontend!!";
  }

  bool SimulatorSettings::runStatus() const { return m_run_status; }

  void SimulatorSettings::setRunStatus(bool run_status)
  {
    m_run_status = run_status;
    emit runStatusChanged(m_run_status);
  }

  bool SimulatorSettings::eMode() const {return m_emode;}

  void SimulatorSettings::seteMode(bool mode)
  {
    m_emode=mode;
    emit eModeChanged(m_emode);
  }

  Qt3DCore::QNodeCreatedChangeBasePtr
  SimulatorSettings::createNodeCreationChange() const
  {
    auto creationChange
      = Qt3DCore::QNodeCreatedChangePtr<SimulatorSettingsInitialData>::create(this);

    auto& data        = creationChange->data;
    data.m_run_status = m_run_status;
    data.m_emode      = m_emode;
    return std::move(creationChange);
  }

}   // namespace rigidbodyaspect


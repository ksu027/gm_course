#ifndef RIGIDBODYASPECT_ENVIRONMENT_H
#define RIGIDBODYASPECT_ENVIRONMENT_H

#include "../constants.h"

// qt
#include <QVector3D>
#include <Qt3DCore/QNode>

namespace rigidbodyaspect
{


  class Environment : public Qt3DCore::QNode {
    Q_OBJECT

    Q_PROPERTY(
      QVector3D gravity READ gravity WRITE setGravity NOTIFY gravityChanged)

  public:
    explicit Environment(Qt3DCore::QNode* parent = nullptr);


    const QVector3D& gravity() const;
    void             setGravity(const QVector3D& gravity);


  signals:
    void gravityChanged(const QVector3D& gravity);

  private:
    QVector3D m_gravity{0, float(-constants::Gn / 100.0), 0};

    // QNode interface
  private:
    Qt3DCore::QNodeCreatedChangeBasePtr
    createNodeCreationChange() const override;
  };


  struct EnvironmentInitialData {
    QVector3D m_gravity;
  };

}   // namespace rigidbodyaspect




#endif   // RIGIDBODYASPECT_ENVIRONMENT_H

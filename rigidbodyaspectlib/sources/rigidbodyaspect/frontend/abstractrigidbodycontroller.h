#ifndef RIGIDBODYASPECT_ABSTRACTRIGIDBODYCONTROLLER_H
#define RIGIDBODYASPECT_ABSTRACTRIGIDBODYCONTROLLER_H


#include "environment.h"
#include "../types.h"

// blaze
#include <blaze/Math.h>

// qt
#include <QMatrix4x4>
#include <Qt3DCore/QComponent>
#include <Qt3DCore/QTransform>

namespace rigidbodyaspect
{

  class AbstractRigidBodyController : public Qt3DCore::QComponent {
    Q_OBJECT

    Q_PROPERTY(QMatrix3x3 dupFrame READ dupFrame NOTIFY dupFrameChanged)
    Q_PROPERTY(QVariant dynamicsType READ dynamicsType WRITE setDynamicsType
                 NOTIFY dynamicsTypeChanged)

    Q_PROPERTY(Environment* environment READ environment WRITE setEnvironment
                 NOTIFY environmentChanged)

  public:
    ~AbstractRigidBodyController() override = default;

    QVariant dynamicsType() const;
    void     setDynamicsType(const QVariant& dynamics_type);

    QMatrix3x3       dupFrame() const;
    Q_INVOKABLE void resetFrameByDup(const QVector3D& dir, const QVector3D& up,
                                     const QVector3D& pos);

    Environment* environment() const;
    void         setEnvironment(Environment* environment);


  protected:
    RigidBodyDynamicsType m_dynamics_type{RigidBodyDynamicsType::DynamicObject};
    QMatrix3x3            m_dup_frame;
    Environment*          m_environment;

  signals:
    void frameComputed(const QVector3D& dir, const QVector3D& up,
                       const QVector3D& pos);
    void dynamicsTypeChanged(RigidBodyDynamicsType dynamics_type);
    void dupFrameChanged(const QMatrix3x3& dupFrame);
    void environmentChanged(Environment* environment);

    // QNode interface
  protected:
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr& change) override;

  protected:
    AbstractRigidBodyController(Qt3DCore::QNode* parent = nullptr);
  };

  struct AbstractRigidBodyInitialData {
    RigidBodyDynamicsType m_dynamics_type;
    QMatrix3x3            m_dup_frame;
    Qt3DCore::QNodeId     m_environment_id;
  };

}   // namespace rigidbodyaspect


#endif   // RIGIDBODYASPECT_ABSTRACTRIGIDBODYCONTROLLER_H

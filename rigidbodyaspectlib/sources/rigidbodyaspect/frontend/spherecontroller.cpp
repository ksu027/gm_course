#include "spherecontroller.h"

namespace rigidbodyaspect
{




  SphereController::SphereController(Qt3DCore::QNode* parent)
    : AbstractRigidBodyController(parent)
  {
  }

  Qt3DCore::QNodeCreatedChangeBasePtr
  SphereController::createNodeCreationChange() const
  {
    auto creationChange
      = Qt3DCore::QNodeCreatedChangePtr<SphereInitialData>::create(this);

    auto& data            = creationChange->data;
    data.m_dynamics_type  = m_dynamics_type;
    data.m_dup_frame      = m_dup_frame;
    data.m_environment_id = Qt3DCore::qIdForNode(m_environment);

    data.m_radius   = m_radius;
    data.m_mass     = m_mass;
    data.m_friction = m_friction;
    data.m_velocity = m_velocity;
    data.m_gravity  = m_gravity;

    return std::move(creationChange);
  }

}   // namespace rigidbodyaspect

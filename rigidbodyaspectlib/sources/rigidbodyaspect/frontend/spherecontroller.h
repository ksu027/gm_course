#ifndef RIGIDBODYASPECT_SPHERECONTROLLER_H
#define RIGIDBODYASPECT_SPHERECONTROLLER_H


#include "abstractrigidbodycontroller.h"

namespace rigidbodyaspect
{

  class SphereController : public AbstractRigidBodyController {
    Q_OBJECT

    Q_PROPERTY(float radius MEMBER m_radius NOTIFY radiusChanged)
    Q_PROPERTY(float mass MEMBER m_mass NOTIFY massChanged)
    Q_PROPERTY(float friction MEMBER m_friction NOTIFY frictionChanged)
    Q_PROPERTY(QVector3D velocity MEMBER m_velocity NOTIFY velocityChanged)
    Q_PROPERTY(QVector3D gravity MEMBER m_gravity NOTIFY gravityChanged)

  public:
    SphereController(Qt3DCore::QNode* parent = nullptr);


    float m_radius{1.0f};
    float m_mass{1.0f};
    float m_friction{0.9f};
    QVector3D m_velocity{0.0f, 0.0f, 0.0f};
    QVector3D m_gravity{0, float(-constants::Gn / 100.0), 0};

  signals:
    void radiusChanged(float radius);
    void massChanged(float mass);
    void frictionChanged(float friction);
    void velocityChanged(const QVector3D& velocity);
    void gravityChanged(const QVector3D& gravity);

    // QNode interface
  private:
    Qt3DCore::QNodeCreatedChangeBasePtr createNodeCreationChange() const override;
  };


  struct SphereInitialData : AbstractRigidBodyInitialData {
    float m_radius;
    float m_mass;
    float m_friction;
    QVector3D m_velocity;
    QVector3D m_gravity;
  };

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_SPHERECONTROLLER_H

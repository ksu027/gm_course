#include "environment.h"


namespace rigidbodyaspect
{

  Environment::Environment(Qt3DCore::QNode* parent) : QNode(parent) {}

  const QVector3D& Environment::gravity() const { return m_gravity; }

  void Environment::setGravity(const QVector3D& gravity)
  {
    m_gravity = gravity;
    gravityChanged(m_gravity);
  }



  Qt3DCore::QNodeCreatedChangeBasePtr
  Environment::createNodeCreationChange() const
  {
    auto creationChange
      = Qt3DCore::QNodeCreatedChangePtr<EnvironmentInitialData>::create(this);

    auto& data     = creationChange->data;
    data.m_gravity = m_gravity;
    return std::move(creationChange);
  }


}   // namespace rigidbodyaspect

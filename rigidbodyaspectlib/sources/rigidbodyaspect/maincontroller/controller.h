#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "../types.h"
#include "../geometry/rbtypes.h"
#include "../constants.h"

#include <variant>
#include <chrono>
#include <vector>
#include <iostream>

namespace rigidbodyaspect::controller
{

/* NOT IN USE */


   // struct SpFplCollisionObject {
   //   Sphere&
   //   const FixedPlane&
   //   second_type t
   // }


struct CollisionObject {
    MovingBody& obj0;
    GM2SpaceObjectType& obj1; /* either moving or fixed*/
    //PhysicsObject* obj1;
    bool obj1_moving;
    seconds_type t; };

  using seconds_type = std::chrono::duration<double>;
  class Controller
  {
  public:

    void collisionDetection(seconds_type dt);
    Controller();
    void fillCollisionObject(const MovingBody* in_obj0, const GM2SpaceObjectType* in_obj1,
                             const bool in_obj1_moving, const seconds_type in_t);
  private:
    std::vector<MovingBody*> D;
    std::vector<FixedBody*> S;
    std::deque<CollisionObject> C;
};

}
#endif // CONTROLLER_H

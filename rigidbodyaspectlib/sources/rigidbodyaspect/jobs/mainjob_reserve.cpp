#include "mainjob.h"

#include "../constants.h"
#include "../backend/rigidbodyaspect.h"
#include "../backend/environmentbackend.h"

#include "../algorithms/collision.h"
#include "../algorithms/simulation.h"


// qt
#include <QDebug>
#include <tuple>
#include <chrono>
#include <cmath>
#include <iostream>
using namespace std::chrono_literals;

namespace rigidbodyaspect
{
  bool sortByTimeC(const rigidbodyaspect::tools::MSphFPlCollisionObject &i, const rigidbodyaspect::tools::MSphFPlCollisionObject &j)
      {return std::get<2>(i) < std::get<2>(j);};

  bool sortByTimeAllC(const rigidbodyaspect::tools::CollisionObject &i, const rigidbodyaspect::tools::CollisionObject &j)
      {return std::get<2>(i) < std::get<2>(j);};

  MainJob::MainJob(RigidBodyAspect* aspect) : m_aspect{aspect} {}

  void MainJob::setFrameTimeDt(seconds_type dt) {m_dt = dt;}

  void MainJob::run()
  {

    std::vector<rigidbodyaspect::tools::CollisionObject> all_collisions;
    algorithms::collision::detail::SphereFixedPlaneColliderReturnType col_res_spsp;

    auto& rigid_bodies = m_aspect->rigidBodies();

    /* create vector of all collision objects SpSp and SpPl */
    all_collisions = algorithms::collision::makeAllCollisionsVector(m_aspect,m_dt);

    /* sort vector of all collision objects SpSp and SpPl */
    std::sort(all_collisions.begin(),all_collisions.end(),sortByTimeAllC);

    /* unique vector of all collision objects SpSp and SpPl */
    rigidbodyaspect::algorithms::collision::deleteDuplicateAllCols(all_collisions);

    /* loop of sorted and unique collisions */
    size_t i = 0;
    while (i<all_collisions.size())
    {
      auto collision_out=all_collisions[i];
      rbtypes::Sphere* col_sphere=(std::get<0>(collision_out))[0];
      if (!(std::get<1>(collision_out)).empty())
      {
        const rbtypes::FixedPlane* col_plane=(std::get<1>(collision_out))[0];
        seconds_type col_t = std::get<2>(collision_out)-(*col_sphere).m_cur_t;

      /* simulate col obj till time from collision tc */
        const auto env_id = (*col_sphere).m_env_id;
        const auto env_BE = m_aspect->environmentBackend(env_id);
        const auto Gn     = env_BE ? env_BE->m_gravity : GM2Vector{0, 20.0, 0};
        const auto F      = Gn;
        const auto a = F * col_t.count();
        const auto ds = ((*col_sphere).m_velocity + 0.5 * a) * col_t.count();
//        const auto ds = rigidbodyaspect::algorithms::collision::computeDS(m_aspect,*col_sphere,col_t);

        (*col_sphere).translateLocal(ds);
        (*col_sphere).m_velocity += a;

      /* compute impact reponse */
      cashcompute::newVelocitySpherePlane(*col_sphere,*col_plane);

      /* update cache properties */
      cashcompute::updateCurT(*col_sphere,col_t+(*col_sphere).m_cur_t);

      /* check for state change <ds,n> ? */
        const auto n = std::get<3>(rigidbodyaspect::tools::getPlaneProps(*col_plane));
        const auto ds_new = ((*col_sphere).m_velocity + 0.5 * F * (m_dt).count()) * (m_dt).count();
        const auto dsn = blaze::inner(ds_new,n);

        /* rest */
        /* compensate gravity to make sum of vectors = 0 */
        const auto is_rest = abs(blaze::inner(-n*(*col_sphere).m_radius,ds)-blaze::inner(ds,ds));
        std::cout<<"is rest = "<<is_rest<<'\n';
        if (is_rest<rigidbodyaspect::constants::eps_double)
        {
          std::cout<<"rest\n";
          (*col_sphere).m_state=rigidbodyaspect::SphereState::Rest;
          (*col_sphere).m_velocity=-0.5 * F * m_dt.count();
        }

        /* sphere attached to the plane */
        if (dsn<0.0)
        {
          std::cout<<"slide\n";
          (*col_sphere).m_velocity=((*col_sphere).m_velocity-0.5 * F * (m_dt).count())*(*col_sphere).m_friction;
//                (*col_sphere).m_velocity=0.5 * F * (m_dt-col_t).count() + rigidbodyaspect::cashcompute::adjDsSpherePlaneSlide(*col_sphere,*col_plane,ds,n)/(m_dt-col_t).count();
        };

      /* inner loop new */
      std::vector<rigidbodyaspect::tools::CollisionObject> collision_in;
      std::vector<rbtypes::Sphere*> Sp;
      Sp.emplace_back((std::get<0>(collision_out))[0]);
      for (auto& fixed_plane : rigid_bodies.fixedPlanes())
        {
          const auto count_t  = m_dt-(*col_sphere).m_cur_t;
          col_res_spsp = algorithms::collision::detectCollisionF((*col_sphere),fixed_plane,count_t,F);
          if (col_res_spsp.first == algorithms::collision::detail::SphereFixedPlaneColliderStatus::Collision)
          {
            if (&fixed_plane!=(std::get<1>(collision_out))[0])
            {
              tools::Planes Pl;
              Pl.emplace_back(&fixed_plane);
              collision_in.emplace_back(Sp,Pl,(col_res_spsp.second*count_t+(*col_sphere).m_cur_t));
            }
          }
      }

      /* sort vector of new collisions after the inner loop */
      std::sort(collision_in.begin(),collision_in.end(),sortByTimeAllC);

      /* make unique inner vector */
      rigidbodyaspect::algorithms::collision::deleteDuplicateAllCols(collision_in);

      /* combine two vectors */
      all_collisions.insert(all_collisions.end(),collision_in.begin(),collision_in.end());
      /* sort vector of all collisions after the inner loop */
      std::sort(all_collisions.begin(),all_collisions.end(),sortByTimeAllC);
      }
    i++;
    }


    /* simulate all the objects till the end */
    for (auto& sphere : rigid_bodies.spheres())
    {
        const auto env_id_o = sphere.m_env_id;
        const auto env_BE_o = m_aspect->environmentBackend(env_id_o);
        const auto Gn_o     = env_BE_o ? env_BE_o->m_gravity : GM2Vector{0, 20.0, 0};
        const auto F_o      = Gn_o;
        const auto count_t  = m_dt-sphere.m_cur_t;
        algorithms::simulation::simulateGenericMovingBody(sphere, count_t, F_o);
    }

  }
}

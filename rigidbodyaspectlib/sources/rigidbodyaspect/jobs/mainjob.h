#ifndef MAINJOB_H
#define MAINJOB_H

#include "../types.h"
#include "../tools.h"
#include "../geometry/rbtypes.h"
#include "../constants.h"
#include "../cashcompute.h"

// qt
#include <Qt3DCore/QAspectJob>
#include <QSharedPointer>

#include <vector>
#include <set>
//#include <utility>

namespace rigidbodyaspect
{


//   using  MSphFPlCollisionObject = std::tuple<rbtypes::Sphere*,const rbtypes::FixedPlane*,seconds_type>;

   using SphereSet = std::set<rbtypes::Sphere*>;

   /* for sorting the vector of collisions by the time (third instance in the tuple)*/
   bool sortByTimeC(const rigidbodyaspect::tools::MSphFPlCollisionObject &i, const rigidbodyaspect::tools::MSphFPlCollisionObject &j);

   class RigidBodyAspect;

   class MainJob : public Qt3DCore::QAspectJob {
   public:
     MainJob(RigidBodyAspect* aspect);
     void setFrameTimeDt(seconds_type dt);

     void run() override;

   private:
     RigidBodyAspect* m_aspect;
     seconds_type     m_dt;

  };

  using MainJobPtr = QSharedPointer<MainJob>;

}



#endif // MAINJOB_H

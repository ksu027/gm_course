#include "genericcolliderjob.h"

#include "../constants.h"
#include "../backend/rigidbodyaspect.h"
#include "../backend/environmentbackend.h"

#include "../cashcompute.h"

// qt
#include <QDebug>

namespace rigidbodyaspect
{

  GenericColliderJob::GenericColliderJob(RigidBodyAspect* aspect)
    : m_aspect{aspect}
  {
  }

  void GenericColliderJob::setFrameTimeDt(seconds_type dt) { m_dt = dt; }

  void GenericColliderJob::run()
  {
    auto& rigid_bodies = m_aspect->rigidBodies();

    for (auto& sphere : rigid_bodies.spheres()) {
      for (auto& fixed_plane : rigid_bodies.fixedPlanes()) {
        if (algorithms::collision::detectCollision(sphere, fixed_plane,m_dt).first
            == algorithms::collision::detail::SphereFixedPlaneColliderStatus::Collision)
        {
            cashcompute::newVelocitySpherePlane(sphere, fixed_plane);
        }
      }
    }
  }

}   // namespace rigidbodyaspect

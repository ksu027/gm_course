#include "mainjob.h"

#include "../constants.h"
#include "../backend/rigidbodyaspect.h"
#include "../backend/environmentbackend.h"

#include "../algorithms/collision.h"
#include "../algorithms/simulation.h"


// qt
#include <QDebug>
#include <tuple>
#include <chrono>
#include <cmath>
#include <iostream>
using namespace std::chrono_literals;

namespace rigidbodyaspect
{
  bool sortByTimeC(const rigidbodyaspect::tools::MSphFPlCollisionObject &i, const rigidbodyaspect::tools::MSphFPlCollisionObject &j)
      {return std::get<2>(i) < std::get<2>(j);};

  bool sortByTimeAllC(const tools::CollisionObject &i, const tools::CollisionObject &j)
      {return std::get<2>(i) < std::get<2>(j);};

  MainJob::MainJob(RigidBodyAspect* aspect) : m_aspect{aspect} {}

  void MainJob::setFrameTimeDt(seconds_type dt) {m_dt = dt;}

  void MainJob::run()
  {
    std::vector<rigidbodyaspect::tools::CollisionObject> all_collisions;
    auto& rigid_bodies = m_aspect->rigidBodies();

    /* CREATE vector of all collision objects SpSp and SpPl */
    all_collisions = algorithms::collision::makeAllCollisionsVector(rigid_bodies,m_dt,*m_aspect);

    /* SORT vector of all collision objects SpSp and SpPl */
    std::sort(all_collisions.begin(),all_collisions.end(),sortByTimeAllC);

    /* UNIQUE vector of all collision objects SpSp and SpPl */
    rigidbodyaspect::algorithms::collision::deleteDuplicateAllCols(all_collisions,0);

    /* MAIN LOOP of sorted and unique collisions */
    size_t i = 0;
    while (i<all_collisions.size())
    {
      auto collision_out=all_collisions[i];
      QMap<char,rbtypes::Sphere*> spheres;

      spheres.insert('a',std::get<0>(collision_out)); //a - active sphere
      seconds_type col_t = std::get<2>(collision_out)-(*spheres.value('a')).m_cur_t;
      /* if SpSp then fill up map */
      if (std::get<3>(collision_out)==tools::ColType::SpSp)
        spheres.insert('p',static_cast<rbtypes::Sphere*>(std::get<1>(collision_out))); //p - passive sphere

      /* SIMULATE moving collision objects till the time of collision */
      for (auto& sphere:spheres)
      {
        /* part of ds with the respect to time of it computation */
        GM2Vector ds = (*sphere).m_ds*(col_t/(m_dt-(*sphere).m_cur_t));

        /* move sphere to the time of collision in space */
        sphere->translateParent(ds);

        /* update velocity to the time of collision */
        cashcompute::updateVelocityAll(*sphere,col_t,ds,*m_aspect);
      }

      /* compute IMPACT RESPONSE */
      cashcompute::newVelocityCollisionObject(collision_out,*m_aspect);

      for (auto& sphere:spheres)
      {
        /* UPDATE cache properties */
        cashcompute::updateCurT(*sphere,col_t+(*sphere).m_cur_t);
        cashcompute::updateDS(*sphere,(m_dt-(*sphere).m_cur_t));
        /* adjust DS if sliding */
        cashcompute::adjustDs(*sphere,*m_aspect);

        if (std::get<3>(collision_out)==tools::ColType::SpSp)
            cashcompute::stateChange(*sphere,nullptr,(m_dt-(*sphere).m_cur_t),*m_aspect);
        else
            cashcompute::stateChange(*sphere,(static_cast<rbtypes::FixedPlane*>(std::get<1>(collision_out))),(m_dt-(*sphere).m_cur_t),*m_aspect);
        cashcompute::stateChangeMultiPlane(*sphere,*m_aspect);
      }

      /* check state change with the respect to sphere */
      /* experimental. in process*/
      if (std::get<3>(collision_out)==tools::ColType::SpSp and m_aspect->eMode())
        cashcompute::stateChangeSP(*spheres['a'],*spheres['p'],(m_dt-(*spheres.value('a')).m_cur_t),*m_aspect);

      std::vector<rigidbodyaspect::tools::CollisionObject> collision_in;
      for (auto& sphere:spheres)
      {
        if ((*sphere).m_state!=SphereState::Rest)
        {
        /* INNNER LOOP */
        std::vector<rigidbodyaspect::tools::CollisionObject>
                collision_tmp = algorithms::collision::findCollisions(*sphere,m_dt,rigid_bodies,*m_aspect);
        collision_in.insert(collision_in.end(),collision_tmp.begin(),collision_tmp.end());
        }
      }

      /* combine two vectors */
      all_collisions.insert(all_collisions.end(),collision_in.begin(),collision_in.end());

      /* sort vector of all collisions after the inner loop */
      std::sort(all_collisions.begin(),all_collisions.end(),sortByTimeAllC);

      /* make unique vector */
      rigidbodyaspect::algorithms::collision::deleteDuplicateAllCols(all_collisions,int(i+1));

      i++;
    }
    /* simulate all the moving objects except REST till the end */
    for (auto& sphere : rigid_bodies.spheres())
    {
      if (sphere.m_state!=rigidbodyaspect::SphereState::Rest)
      {
        const auto count_t  = m_dt-sphere.m_cur_t;
        algorithms::simulation::simulateGenericMovingBody(sphere, count_t,*m_aspect);
      }
    }
  }
}

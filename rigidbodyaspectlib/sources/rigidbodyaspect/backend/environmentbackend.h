#ifndef RIGIDBODYASPECT_ENVIRONMENTBACKEND_H
#define RIGIDBODYASPECT_ENVIRONMENTBACKEND_H


#include "../types.h"

#include <Qt3DCore/QBackendNode>

namespace rigidbodyaspect
{
  class EnvironmentBackend : public Qt3DCore::QBackendNode {
  public:
    EnvironmentBackend();

    GM2Vector m_gravity;


    // QBackendNode interface
  protected:
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e) override;

  private:
    void initializeFromPeer(
      const Qt3DCore::QNodeCreatedChangeBasePtr& change) override;
  };



  class RigidBodyAspect;

  class EnvironmentBackendMapper : public Qt3DCore::QBackendNodeMapper {
  public:
    explicit EnvironmentBackendMapper(RigidBodyAspect* aspect);

  private:
    RigidBodyAspect* m_aspect;

    // QBackendNodeMapper interface
  public:
    Qt3DCore::QBackendNode*
                            create(const Qt3DCore::QNodeCreatedChangeBasePtr& change) const override;
    Qt3DCore::QBackendNode* get(Qt3DCore::QNodeId id) const override;
    void                    destroy(Qt3DCore::QNodeId id) const override;

//    void                    clearMaps() const;
  };




}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_ENVIRONMENTBACKEND_H

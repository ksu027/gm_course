#ifndef RIGIDBODYASPECT_SIMULATORSETTINGSBACKEND_H
#define RIGIDBODYASPECT_SIMULATORSETTINGSBACKEND_H



// qt
#include <Qt3DCore/QBackendNode>



namespace rigidbodyaspect
{

  class SimulatorSettingsBackend : public Qt3DCore::QBackendNode {
  public:
    SimulatorSettingsBackend();


    bool runStatus() const;
    bool eMode() const;

  private:
    bool m_run_status;
    bool m_emode;

    // QBackendNode interface
  protected:
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e) override;

  private:
    void initializeFromPeer(
      const Qt3DCore::QNodeCreatedChangeBasePtr& change) override;
  };



  class RigidBodyAspect;

  class SimulatorSettingsBackendMapper : public Qt3DCore::QBackendNodeMapper {
  public:
    explicit SimulatorSettingsBackendMapper(RigidBodyAspect* aspect);

  private:
    RigidBodyAspect* m_aspect;

    // QBackendNodeMapper interface
  public:
    Qt3DCore::QBackendNode*
                            create(const Qt3DCore::QNodeCreatedChangeBasePtr& change) const override;
    Qt3DCore::QBackendNode* get(Qt3DCore::QNodeId id) const override;
    void                    destroy(Qt3DCore::QNodeId id) const override;
  };


}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_SIMULATORSETTINGSBACKEND_H

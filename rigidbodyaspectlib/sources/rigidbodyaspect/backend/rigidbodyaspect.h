#ifndef RIGIDBODYASPECT_RIGIDBODYASPECT_H
#define RIGIDBODYASPECT_RIGIDBODYASPECT_H


#include "../jobs/genericsimjob.h"
#include "../jobs/genericupdatejob.h"
#include "../jobs/mainjob.h"

#include "../geometry/rigidbodycontainer.h"

// qt
#include <QPair>
#include <Qt3DCore>

namespace rigidbodyaspect
{
  class EnvironmentBackend;
  class SimulatorSettingsBackend;
  class SphereControllerBackend;
  class PlaneControllerBackend;

  class RigidBodyAspect : public Qt3DCore::QAbstractAspect {
    Q_OBJECT
  public:
    // Types
    using SimulatorSettingsBackendSP = QSharedPointer<SimulatorSettingsBackend>;
    using EnvironmentBackendQHash
      = QHash<Qt3DCore::QNodeId, EnvironmentBackend*>;
    using SphereControllerBackendQHash
      = QHash<Qt3DCore::QNodeId, SphereControllerBackend*>;
    using PlaneControllerBackendQHash
      = QHash<Qt3DCore::QNodeId, PlaneControllerBackend*>;

    // Constructor
    explicit RigidBodyAspect(QObject* parent = nullptr);


    ////////////////////////////
    // SimulatorSettings Backend
    SimulatorSettingsBackend*
                              constructSimulatorSettingsBackend(Qt3DCore::QNodeId id);
    SimulatorSettingsBackend* simulatorSettingsBackend(Qt3DCore::QNodeId id);
    void releaseSimulatorSettingsBackend(Qt3DCore::QNodeId id);


    //////////////////////
    // Environment Backend
    void                addEnvironmentBackend(Qt3DCore::QNodeId   id,
                                              EnvironmentBackend* backend);
    EnvironmentBackend* environmentBackend(Qt3DCore::QNodeId id);
    EnvironmentBackend* takeEnvironmentBackend(Qt3DCore::QNodeId id);
    const QHash<Qt3DCore::QNodeId, EnvironmentBackend*>&
    environmentBackends() const;

    ////////////////////////////////////////
    // Rigidbody Controller Backend : Sphere
    void                     addSphereControllerBackend(Qt3DCore::QNodeId        id,
                                                        SphereControllerBackend* backend);
    SphereControllerBackend* sphereControllerBackend(Qt3DCore::QNodeId id);
    SphereControllerBackend* takeSphereControllerBackend(Qt3DCore::QNodeId id);
    const QHash<Qt3DCore::QNodeId, SphereControllerBackend*>&
    sphereControllerBackends() const;

    ///////////////////////////////////////
    // Rigidbody Controller Backend : Plane
    void                    addPlaneControllerBackend(Qt3DCore::QNodeId       id,
                                                      PlaneControllerBackend* backend);
    PlaneControllerBackend* planeControllerBackend(Qt3DCore::QNodeId id);
    PlaneControllerBackend* takePlaneControllerBackend(Qt3DCore::QNodeId id);
    const QHash<Qt3DCore::QNodeId, PlaneControllerBackend*>&
    planeControllerBackends() const;


    /////////////////
    // Object Manager
    RigidBodyContainer& rigidBodies();

    /* to store pairs of attached bodies */
    rbtypes::AttachSpSp& spspRest();
    rbtypes::AttachSpSp& spspSlide();
    rbtypes::AttachSpPl& spplRest();
    rbtypes::AttachSpPl& spplSlide();
    void clearMaps(Qt3DCore::QNodeId id);

    bool eMode();
  private:
    SimulatorSettingsBackendSP m_simulator_settings_backend{nullptr};
    Qt3DCore::QNodeId          m_simulator_settings_backend_peerid;

    EnvironmentBackendQHash m_environment_backends;

    SphereControllerBackendQHash m_spherecontroller_backends;
    PlaneControllerBackendQHash  m_planecontroller_backends;

    RigidBodyContainer m_rigid_bodies;

    rbtypes::AttachSpSp m_spsp_rest;
    rbtypes::AttachSpSp m_spsp_slide;
    rbtypes::AttachSpPl m_sppl_rest;
    rbtypes::AttachSpPl m_sppl_slide;

    bool m_emode = false;

    qint64                m_last_time{0};
    MainJobPtr            m_mainworker;
    GenericUpdateJobPtr   m_updateworker;

    // QAbstractAspect interface
  private:
    QVector<Qt3DCore::QAspectJobPtr> jobsToExecute(qint64 time) override;

  };

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_RIGIDBODYASPECT_H

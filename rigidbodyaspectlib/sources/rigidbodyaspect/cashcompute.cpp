#include "cashcompute.h"

#include "backend/rigidbodyaspect.h"

using namespace std::chrono_literals;

namespace rigidbodyaspect::cashcompute {

    void newVelocitySpherePlane(rbtypes::Sphere&     sphere,
                                 const rbtypes::FixedPlane& plane)
    {
        /* cover in the function? */
        const auto pl_eval = plane.evaluateParent(rbtypes::FixedPlane::PSpacePoint{0.0,0.0},
                                                  rbtypes::FixedPlane::PSizeArray{1UL,1UL});
        const auto pl_u = blaze::subvector<0UL,3UL>(pl_eval(1UL,0UL));
        const auto pl_v = blaze::subvector<0UL,3UL>(pl_eval(0UL,1UL));

        const auto n = blaze::normalize(blaze::cross(pl_u,pl_v));

        /* dummy friction */
        sphere.m_velocity=(sphere.m_velocity-2*blaze::inner(sphere.m_velocity,n)*n)*sphere.m_friction;

        return ;
    };

    void newVelocitySphereSphere(rbtypes::Sphere& act_sphere,
                                 rbtypes::Sphere& pas_sphere,
                                 RigidBodyAspect& aspect)
    {
              qDebug()<<"col SPSP";
      const auto p0 = act_sphere.frameOriginParent();
      const auto p1 = pas_sphere.frameOriginParent();
      const auto d  = (p1-p0)/blaze::length(p1-p0);

      const auto liv_d = gmlib2::algorithms::linearIndependentVector(GM2Vector(d));

      const auto n  = blaze::cross(liv_d,d)/blaze::length((blaze::cross(liv_d,d)));

      const auto v0 = act_sphere.m_velocity;
      const auto v1 = pas_sphere.m_velocity;

      const auto v0d = blaze::inner(v0,d);
      const auto v1d = blaze::inner(v1,d);
      const auto v0n = blaze::inner(v0,n);
      const auto v1n = blaze::inner(v1,n);

      const auto m0  = act_sphere.m_mass;
      const auto m1  = pas_sphere.m_mass;

      const auto v0d_new = (m0-m1)/(m0+m1)*v0d + 2*m1*v1d/(m0+m1);
      const auto v1d_new = (m1-m0)/(m0+m1)*v1d + 2*m0*v0d/(m0+m1);

      const auto new_act_vel = v0n*n+v0d_new*d;
      const auto new_pas_vel = v1n*n+v1d_new*d;

      /* special case, when ball falling directly on another REST ball
       * at 90' to one of the resting planes->norm of ball collision
       * parallel to the norm of attached plane of resting ball */
      if (pas_sphere.m_state==SphereState::Rest)
      {
        const auto n_check = blaze::normalize(d);
        for (const auto& attach_plane:aspect.spplRest().values(&pas_sphere))
        {
          const auto n_att = std::get<3>(rigidbodyaspect::tools::getPlaneProps(*attach_plane));
          const auto change_response = abs(blaze::length(blaze::cross(n_check,n_att)));
          if (change_response<constants::eps_double)
          {
            /* give all velocity back to act sphere in opposite direction */
            //act_sphere.m_velocity = (new_act_vel - new_pas_vel)*act_sphere.m_friction;
            /* change the direction of the resting sphere velocity */
            act_sphere.m_velocity = new_act_vel*act_sphere.m_friction;
            pas_sphere.m_velocity = -new_pas_vel*pas_sphere.m_friction;
            return;
          }
        }
      }

      /* with dummy friction */
      act_sphere.m_velocity = new_act_vel*act_sphere.m_friction;
      pas_sphere.m_velocity = new_pas_vel*pas_sphere.m_friction;

      /* could be added modification: when collide with a resting sphere
       * multiply weight of resting sphere on tan of angle between normals
       * of resting plane and colliding imaginary plane
       * for the computations of response of another, free moving sphere */
      return;
    };

    void newVelocityCollisionObject(tools::CollisionObject& collision, RigidBodyAspect& aspect)
    {
      if (std::get<3>(collision)==tools::ColType::SpSp)
      {
        newVelocitySphereSphere(*(std::get<0>(collision)),*(static_cast<rbtypes::Sphere*>(std::get<1>(collision))),aspect);
      }
      else
      {
        newVelocitySpherePlane(*(std::get<0>(collision)),*(static_cast<rbtypes::FixedPlane*>(std::get<1>(collision))));
        /* if sphere is attached to one plane, check the angle
         * if it is 90" - adjust velocity. Special case */
        VelocityAdjust(*(std::get<0>(collision)),*(static_cast<rbtypes::FixedPlane*>(std::get<1>(collision))), aspect);
      }

    };

    void VelocityAdjust(rbtypes::Sphere& sphere, rbtypes::FixedPlane& response_plane, RigidBodyAspect& aspect)
    {
      /* if sphere attached to the plane */
      if (aspect.spplSlide().contains(&sphere))
      {
        rbtypes::FixedPlane& attached_plane = *aspect.spplSlide().find(&sphere).value();
        auto n_attached = std::get<3>(rigidbodyaspect::tools::getPlaneProps(attached_plane));
        auto n_response  = std::get<3>(rigidbodyaspect::tools::getPlaneProps(response_plane));
        /* check the angle of two planes, if it is 90"- special case */
        const auto adjust_velocity = abs(blaze::inner(n_attached,n_response));
        if (adjust_velocity<rigidbodyaspect::constants::eps_double)
        {
          /* fix. Must be changed. Now forcing to stop it. 0.4 - found experimentally  */
          if (blaze::length(sphere.m_velocity)<0.4)
              sphere.m_velocity={0.0,0.0,0.0};
        }
      }
    };

    void stateChangeGeneral(rbtypes::Sphere& sphere, seconds_type dt, RigidBodyAspect& aspect)
    {
      /* check if there is a state change slide to rest or rest to not rest */
      if (!((aspect.spplSlide().count(&sphere)>1 or aspect.spplRest().count(&sphere)>1 ) or
            (aspect.spplSlide().count(&sphere)>=1 and aspect.spplRest().count(&sphere)>=1)))
      {
        /* slide -> not_slide */
        if (aspect.spplSlide().contains(&sphere))
           stateChange(sphere, aspect.spplSlide().find(&sphere).value(),dt,aspect);
        /* rest -> not_rest */
        else if (aspect.spplRest().contains(&sphere))
           stateChange(sphere, aspect.spplRest().find(&sphere).value(),dt,aspect);
      }
      /* check state change special case touching several planes */
      if ((aspect.spplSlide().count(&sphere)>1 or aspect.spplRest().count(&sphere)>1 ) or
          (aspect.spplSlide().count(&sphere)>=1 and aspect.spplRest().count(&sphere)>=1))
           cashcompute::stateChangeMultiPlane(sphere,aspect);

    }

    void stateChange(rbtypes::Sphere& sphere, void* plane_ptr, const seconds_type dt, RigidBodyAspect& aspect)
    /* call that function only after update DS for all and the adjust of slide DS */
    {

      if (!(sphere.m_state==SphereState::Rest and aspect.spplRest().count(&sphere)>0))
      {
        /* check slide->free
         * check with connected plane, to the colliding sphere */
        if ((sphere.m_state==rigidbodyaspect::SphereState::Slide)
            and (aspect.spplSlide().count(&sphere)>0))
        {
          auto& attach_plane = *aspect.spplSlide().value(&sphere);
          const auto  ds_org = computeDS(sphere,dt);
          const auto   n_org = std::get<3>(rigidbodyaspect::tools::getPlaneProps(attach_plane));
          const auto dsn_org = blaze::inner(ds_org,n_org);
          if ((dsn_org>0.0))
          {
            qDebug()<<"slide->free";
            sphere.m_state=rigidbodyaspect::SphereState::Free;
            aspect.spplSlide().remove(&sphere);
            sphere.m_ds=ds_org;
          }
          /* could be added check slide->rest for already attached plane */
        }
        /* check for inputed plane */
        if (plane_ptr!=nullptr)
        {
        const auto& plane = static_cast<rbtypes::FixedPlane*>(plane_ptr);
        const auto n = std::get<3>(rigidbodyaspect::tools::getPlaneProps(*plane));
        const auto dsn = blaze::inner(sphere.m_ds,n);
        const auto is_rest = abs(blaze::inner(-n*sphere.m_radius,sphere.m_ds)-blaze::inner(sphere.m_ds,sphere.m_ds));
        /* check free->slide, free->rest, slide->rest */
        if (is_rest<rigidbodyaspect::constants::eps_double)
        {
          qDebug()<<"->rest";
          sphere.m_state=rigidbodyaspect::SphereState::Rest;
          sphere.m_velocity={0.0,0.0,0.0};
          sphere.m_ds={0.0,0.0,0.0};
          aspect.spplRest().insertMulti(&sphere,plane);
          aspect.spplSlide().remove(&sphere);
        } else
        if (dsn<=0.0)
        {
          /* check and adjust only if in Free state or sliding to another plane
           * otherwise it will duplicate instance in map */
          if ((sphere.m_state==rigidbodyaspect::SphereState::Free) or
             (plane!=aspect.spplSlide().value(&sphere)))
          {
            qDebug()<<"->slide";
            aspect.spplSlide().insertMulti(&sphere,plane);
            aspect.spplRest().remove(&sphere);
            sphere.m_state=rigidbodyaspect::SphereState::Slide;
            sphere.m_ds=adjDsSpherePlaneSlide(sphere,*plane,sphere.m_ds,n);
          };
        }
        }
      }
      /* check rest->slide rest->free */
      /* for previously attached plane */
      else
      {
        /* rest->not_rest */
        /* if ds isn't parallel to the norm of the touching plane then change from rest */
        /* parallel, if length of cross product gravityDS and n of plane = 0 */
        /* ds is used which computed outside the procedure with updateDS function */
        auto& attach_plane = *aspect.spplRest().value(&sphere);

        GM2Vector n;
        for (auto& plane:aspect.spplRest().values(&sphere))
        {
          auto n_temp = std::get<3>(rigidbodyaspect::tools::getPlaneProps(*plane));
          n+=n_temp;
        }
        n=blaze::normalize(n);
        const auto change_rest = abs(blaze::length(blaze::cross(n,sphere.m_ds)));
        if (change_rest>rigidbodyaspect::constants::eps_double)
        {
          /* remove from sphere plane rest */
          const auto ds_n = blaze::inner(sphere.m_ds,n);
          /* rest->slide */
          if (ds_n<0.0)
          {
            /* change to slide only if connected to one plane otherwise, we have to adjust DS with imaginary normal, which for now is not implemented */
            /* if comment that check, resting between two planes ball after the collision with other one will fly as crazy */
            if (aspect.spplRest().count(&sphere)==1)
            {
              qDebug()<<"rest->slide";
              aspect.spplRest().remove(&sphere);
              aspect.spplSlide().insertMulti(&sphere,&attach_plane);
              sphere.m_state=rigidbodyaspect::SphereState::Slide;
              sphere.m_ds=adjDsSpherePlaneSlide(sphere,attach_plane,sphere.m_ds,n);
            }
          }
          /* rest->free */
          else
          {
            qDebug()<<"rest->free";
            aspect.spplRest().remove(&sphere);
            sphere.m_state=rigidbodyaspect::SphereState::Free;
          }
        }
        /* rest still rest. clean m_ds to 0, as it was changed previously outside the function */
        else
        {
            sphere.m_ds={0.0,0.0,0.0};
            sphere.m_velocity={0.0,0.0,0.0};
        }
      }
    };

    void stateChangeMultiPlane(rbtypes::Sphere& sphere, RigidBodyAspect& aspect)
    {
      /* state change check for sphere with multiple connection to planes */
        if ((sphere.m_state==SphereState::Slide and (aspect.spplSlide().count(&sphere)>1 or aspect.spplRest().count(&sphere)>1 or (aspect.spplSlide().count(&sphere)>=1 and aspect.spplRest().count(&sphere)>=1)))
         or (sphere.m_state==SphereState::Rest  and (aspect.spplRest().count(&sphere)>1 or aspect.spplSlide().count(&sphere)>1 or (aspect.spplSlide().count(&sphere)>=1 and aspect.spplRest().count(&sphere)>=1))))
        {
          qDebug()<<"->rest_multi\n";
          for (auto& plane:aspect.spplSlide().values(&sphere))
          {
            aspect.spplRest().insertMulti(&sphere,plane);
          }
          sphere.m_state=SphereState::Rest;
          aspect.spplSlide().remove(&sphere);
          sphere.m_velocity={0.0,0.0,0.0};
          sphere.m_ds={0.0,0.0,0.0};
        }
    };

    void stateChangeSP(rbtypes::Sphere& act_sphere, rbtypes::Sphere& pas_sphere, const seconds_type dt, RigidBodyAspect& aspect)
    {
        /* experimental function */
        GM2Vector ds_act=act_sphere.m_ds;
        GM2Vector ds_pas=pas_sphere.m_ds;
        const auto p_act = act_sphere.frameOriginParent();
        const auto p_pas = pas_sphere.frameOriginParent();
        const auto dn  = blaze::normalize(p_act+ds_act-(p_pas+ds_pas));
        const auto d_length = blaze::length(p_act+ds_act-(p_pas+ds_pas));
        const auto R = act_sphere.m_radius+pas_sphere.m_radius;

        const auto is_rest = abs(blaze::inner(-dn*act_sphere.m_radius,act_sphere.m_ds)-blaze::inner(act_sphere.m_ds,act_sphere.m_ds));


        const auto  ds_org = computeDS(act_sphere,dt);
        const auto  d_length_org = blaze::length(p_act+ds_org-(p_pas+ds_pas));

        const auto  ds_org_pas = computeDS(pas_sphere,dt);
        /* was sliding on sphere but in the next time step */
        /* to fresSP */
        if (isSlideSp(act_sphere,aspect))
        {

          if (d_length_org>R)
          {
            qDebug()<<" -> freeSP ";
            aspect.spspSlide().remove(&act_sphere);
            act_sphere.m_ds=ds_org;
          }
        }

        /* from free SP to slide SP or rest SP */
        if (act_sphere.m_ds==pas_sphere.m_ds /*std::abs(blaze::inner(ds_org,ds_org)-blaze::inner(pas_sphere.m_ds,pas_sphere.m_ds))<constants::eps_double*/ and  std::abs(d_length-R)<constants::eps_double)
//        if (std::abs(blaze::inner(ds_org,ds_org)-blaze::inner(ds_org_pas,ds_org_pas))<constants::eps_double and  std::abs(d_length-R)<constants::eps_double)
//        if (ds_org_pas+p_act==ds_org+p_pas and  std::abs(d_length-R)<constants::eps_double)
        {
          qDebug()<<" -> restSP ";
          /* who is heavier, is main */
          if (pas_sphere.m_mass>=act_sphere.m_mass)
          {
            act_sphere.m_ds=pas_sphere.m_ds;
            act_sphere.m_velocity=pas_sphere.m_velocity;
            /* dependent sphere first */
            aspect.spspRest().insertMulti(&act_sphere,&pas_sphere);
            aspect.spspSlide().remove(&act_sphere);
          }
          else
          {
            pas_sphere.m_ds=act_sphere.m_ds;
            pas_sphere.m_velocity=act_sphere.m_velocity;
            /* dependent sphere first */
            aspect.spspRest().insertMulti(&pas_sphere,&act_sphere);
            aspect.spspSlide().remove(&pas_sphere);
          }
        }
        else
            if (d_length<R)
          {
            qDebug()<<" -> slideSP ";
            if (pas_sphere.m_mass>=act_sphere.m_mass)
            {
              /* dependent sphere first */
              aspect.spspSlide().insertMulti(&act_sphere,&pas_sphere);
              aspect.spspRest().remove(&act_sphere);
              act_sphere.m_ds=adjDsSpSpSlide(act_sphere,pas_sphere);
            } else
            {
              aspect.spspSlide().insertMulti(&pas_sphere,&act_sphere);
              aspect.spspRest().remove(&pas_sphere);
              pas_sphere.m_ds=adjDsSpSpSlide(pas_sphere,act_sphere);
            }
          }

    };


    GM2Vector adjDsSpherePlaneSlide(rbtypes::Sphere& sphere,
                                const rbtypes::FixedPlane& plane, const GM2Vector& ds, const GM2Vector& n)
    {
      const auto p = sphere.frameOriginParent();
      const auto q = rigidbodyaspect::algorithms::closestpoint::findClosePointOnPlane(plane,ds+p);
      const auto r = sphere.m_radius;
      const auto adjDS = ds+(q-(ds+p))+n*r;
      return adjDS;
    };

    GM2Vector adjDsSpSpSlide(rbtypes::Sphere& act_sphere,
                             rbtypes::Sphere& pas_sphere)
    {
      GM2Vector p_act = act_sphere.frameOriginParent();
      GM2Vector p_pas = pas_sphere.frameOriginParent();
      /* distance betweeen centers of spheres after both of their DSes */
      GM2Vector d = p_act+act_sphere.m_ds - (p_pas + pas_sphere.m_ds);
      const auto d_len = blaze::length(d);
      const auto r_sum = act_sphere.m_radius+pas_sphere.m_radius;
      const auto k = r_sum/d_len;
      GM2Vector adjDS = p_pas + pas_sphere.m_ds + k*d - p_act;
      return adjDS;
    };

    void adjustDs(rbtypes::Sphere& sphere, RigidBodyAspect& aspect)
    {
      /* if sphere is already sliding on the plane - adjust ds */
      if (sphere.m_state==SphereState::Slide and (aspect.spplSlide().count(&sphere)==1))
      {
        const auto& slide_plane = aspect.spplSlide().value(&sphere);
        sphere.m_ds=cashcompute::adjDsSpherePlaneSlide(sphere,*slide_plane,sphere.m_ds,std::get<3>(rigidbodyaspect::tools::getPlaneProps(*slide_plane)));
      }
        /* if sphere is already sliding on the sphere - adjust ds */
        else if (aspect.spspSlide().count(&sphere)==1)
        {
          sphere.m_ds=cashcompute::adjDsSpSpSlide(sphere,*(aspect.spspSlide().value(&sphere)));
        }
    };

    void updateDS (rbtypes::Sphere& sphere, const seconds_type dt_in)
    {
      /* ds=(v+0.5a)t */
      sphere.m_ds = computeDS(sphere,dt_in);
    };

    void updateCurT (rbtypes::Sphere& sphere,seconds_type dt_sec)
    {
      sphere.m_cur_t = dt_sec;
    };

    void updateVelocity(rbtypes::Sphere& sphere, seconds_type dt_sec)
    {
      /* update velocity for the Free state */
      sphere.m_velocity += sphere.m_gravity*dt_sec.count();
    };

    void updateVelocitySlide (rbtypes::Sphere& sphere, seconds_type dt,GM2Vector& ds)
    {
      /* update velocity in Slide state (sphere slides on plane) */
      sphere.m_velocity+=(ds/dt.count()-sphere.m_velocity)/(dt.count()*0.5)*dt.count();
    };
    void updateVelocityAll (rbtypes::Sphere& sphere, seconds_type dt,GM2Vector& ds, RigidBodyAspect& aspect)
    {
      /* General function choose, which other update velocity to call */
      if (sphere.m_state==rigidbodyaspect::SphereState::Slide and !isSlideSp(sphere,aspect))
        cashcompute::updateVelocitySlide(sphere,dt,ds);
      else if (!isRestSp(sphere,aspect))
        cashcompute::updateVelocity(sphere,dt);
      else
      {
        sphere.m_velocity=(*aspect.spspRest().value(&sphere)).m_velocity;
      }
    };

    GM2Vector computeDS(rbtypes::Sphere& sphere, const seconds_type dt_in)
    {
      /* ds=(v+0.5a)t */
      return (sphere.m_velocity + 0.5 * sphere.m_gravity*dt_in.count()) * dt_in.count();
    }

    bool isSlideSp(rbtypes::Sphere& sphere, RigidBodyAspect& aspect)
    {
      return aspect.spspSlide().contains(&sphere);
    };
    bool isRestSp(rbtypes::Sphere& sphere, RigidBodyAspect& aspect)
    {
      return aspect.spspRest().contains(&sphere);
    };

}

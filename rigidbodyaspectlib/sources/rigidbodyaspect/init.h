#ifndef RIGIDBODYASPECT_INIT_H
#define RIGIDBODYASPECT_INIT_H

#include "types.h"
#include "frontend/simulatorsettings.h"
#include "frontend/spherecontroller.h"
#include "frontend/planecontroller.h"
#include "frontend/environment.h"

namespace rigidbodyaspect::init
{

  namespace detail
  {
    constexpr auto qt_registertype_uri = "com.uit.STE6245.RigidBody";
  }   // namespace detail

  void registerQmlTypes()
  {
    qmlRegisterUncreatableMetaObject(rigidbodyaspect::staticMetaObject,
                                     detail::qt_registertype_uri, 1, 0,
                                     "RigidBodyNS", "Error: a namespace ...");

    qmlRegisterType<SphereController>(detail::qt_registertype_uri, 1, 0,
                                      "SphereController");

    qmlRegisterType<PlaneController>(detail::qt_registertype_uri, 1, 0,
                                     "PlaneController");


    qmlRegisterType<SimulatorSettings>(detail::qt_registertype_uri, 1, 0,
                                       "SimulatorSettings");

    qmlRegisterType<Environment>(detail::qt_registertype_uri, 1, 0,
                                 "Environment");
  }


}   // namespace rigidbodyaspect::init

#endif   // RIGIDBODYASPECT_INIT_H

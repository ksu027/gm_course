#ifndef CASHCOMPUTE_H
#define CASHCOMPUTE_H


#include "types.h"
#include "tools.h"
#include "geometry/rbtypes.h"
#include "geometry/movingbody.h"
#include "constants.h"
#include "algorithms/closestpoint.h"

#include <Qt3DCore/QAspectJob>
#include <QSharedPointer>

namespace rigidbodyaspect::cashcompute {

    /* response velocity */
    void newVelocitySpherePlane(rbtypes::Sphere& sphere, const rbtypes::FixedPlane& plane);
    void newVelocitySphereSphere(rbtypes::Sphere& act_sphere, rbtypes::Sphere& pas_sphere, RigidBodyAspect& aspect);
    void newVelocityCollisionObject(tools::CollisionObject& collision, RigidBodyAspect& aspect);
    void VelocityAdjust(rbtypes::Sphere& sphere,rbtypes::FixedPlane& response_plane, RigidBodyAspect& aspect);

    /* state change check */
    void stateChangeGeneral(rbtypes::Sphere& sphere, seconds_type dt,RigidBodyAspect& aspect);
    void stateChange(rbtypes::Sphere& sphere, void* plane, const seconds_type dt, RigidBodyAspect& aspect);
    void stateChangeSP(rbtypes::Sphere& act_sphere, rbtypes::Sphere& pas_sphere, const seconds_type dt, RigidBodyAspect& aspect);
    void stateChangeMultiPlane(rbtypes::Sphere& sphere, RigidBodyAspect& aspect);

    /* ds adjastment for sliding */
    GM2Vector adjDsSpherePlaneSlide(rbtypes::Sphere& sphere, const rbtypes::FixedPlane& plane, const GM2Vector& ds, const GM2Vector& n);
    GM2Vector adjDsSpSpSlide(rbtypes::Sphere& act_sphere, rbtypes::Sphere& pas_sphere);
    void adjustDs(rbtypes::Sphere& sphere, RigidBodyAspect& aspect);

    /* cache update */
    void updateDS (rbtypes::Sphere& sphere, const seconds_type dt_in);
    void updateCurT (rbtypes::Sphere& sphere,seconds_type dt_sec);
    void updateVelocity (rbtypes::Sphere& sphere,seconds_type dt_sec);
    void updateVelocitySlide (rbtypes::Sphere& sphere, seconds_type dt,GM2Vector& ds);
    void updateVelocityAll (rbtypes::Sphere& sphere, seconds_type dt,GM2Vector& ds, RigidBodyAspect& aspect);
    GM2Vector computeDS (rbtypes::Sphere& sphere, const seconds_type dt_in);

    /* check substate */
    bool isSlideSp(rbtypes::Sphere& sphere,RigidBodyAspect& aspect);
    bool isRestSp(rbtypes::Sphere& sphere,RigidBodyAspect& aspect);
}

#endif // CASHCOMPUTE_H

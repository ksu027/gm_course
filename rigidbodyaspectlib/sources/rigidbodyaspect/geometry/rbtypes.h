#ifndef RIGIDBODYASPECT_RBTYPES_H
#define RIGIDBODYASPECT_RBTYPES_H

#include "movingbody.h"
#include "fixedbody.h"

namespace rigidbodyaspect::rbtypes {

    using Sphere     = gmlib2::parametric::PSphere<MovingBody>;
    using FixedPlane = gmlib2::parametric::PPlane<FixedBody>;

    enum class AttType {SpSp,SpPl};

    using AttachSpPl = QMap<rbtypes::Sphere*,rbtypes::FixedPlane*>;
    using AttachSpSp = QMap<rbtypes::Sphere*,rbtypes::Sphere*>;

}



#endif // RIGIDBODYASPECT_RBTYPES_H

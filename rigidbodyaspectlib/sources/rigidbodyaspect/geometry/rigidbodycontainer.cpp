#include "rigidbodycontainer.h"

#include "../utils.h"

namespace rigidbodyaspect
{

  RigidBodyContainer::RigidBodyContainer() {}

  void RigidBodyContainer::constructSphere(Qt3DCore::QNodeId id)
  {
    //    if(m_spheres.contains(id)) throw std::runtime_exception{};
    m_spheres.insert(id, {});
  }

  void RigidBodyContainer::destroySphere(Qt3DCore::QNodeId id)
  {
    //    if(not m_spheres.contains(id)) throw std::runtime_exception{};
    m_spheres.remove(id);
  }

  rbtypes::Sphere& RigidBodyContainer::sphere(Qt3DCore::QNodeId id)
  {
    //    if(not m_spheres.contains(id)) throw std::runtime_exception{};
    return m_spheres[id];
  }

  const rbtypes::Sphere RigidBodyContainer::sphere(Qt3DCore::QNodeId id) const
  {
    //    if(not m_spheres.contains(id)) throw std::runtime_exception{};
    return m_spheres[id];
  }

  RigidBodyContainer::SphereQHash& RigidBodyContainer::spheres()
  {
    return m_spheres;
  }

  const RigidBodyContainer::SphereQHash& RigidBodyContainer::spheres() const
  {
    return m_spheres;
  }

  void RigidBodyContainer::constructFixedPlane(Qt3DCore::QNodeId id)
  {
    //    if(m_fixed_planes.contains(id)) throw std::runtime_exception{};
    m_fixed_planes.insert(id, {});
  }

  void RigidBodyContainer::destroyFixedPlane(Qt3DCore::QNodeId id)
  {
    //    if(not m_fixed_planes.contains(id)) throw std::runtime_exception{};
    m_fixed_planes.remove(id);
  }

  rbtypes::FixedPlane& RigidBodyContainer::fixedPlane(Qt3DCore::QNodeId id)
  {
    //    if(not m_fixed_planes.contains(id)) throw std::runtime_exception{};
    return m_fixed_planes[id];
  }

  const rbtypes::FixedPlane RigidBodyContainer::fixedPlane(Qt3DCore::QNodeId id) const
  {
    //    if(not m_fixed_planes.contains(id)) throw std::runtime_exception{};
    return m_fixed_planes[id];
  }

  RigidBodyContainer::FixedPlaneQHash& RigidBodyContainer::fixedPlanes()
  {
    return m_fixed_planes;
  }

  const RigidBodyContainer::FixedPlaneQHash& RigidBodyContainer::fixedPlanes() const
  {
    return m_fixed_planes;
  }

}   // namespace rigidbodyaspect

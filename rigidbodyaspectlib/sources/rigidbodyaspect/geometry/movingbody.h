#ifndef RIGIDBODYASPECT_MOVINGBODY_H
#define RIGIDBODYASPECT_MOVINGBODY_H

#include "../types.h"

using namespace std::chrono_literals;

namespace rigidbodyaspect
{

  class MovingBody : public GM2SpaceObjectType  {
  public:

    // members
    Unit_Type         m_mass;
    Unit_Type         m_friction;
    Qt3DCore::QNodeId m_env_id;
    SphereState       m_state;

    GM2Vector         m_ds;
    seconds_type      m_cur_t;
    GM2Vector         m_velocity;
    GM2Vector         m_gravity;

  };

}   // namespace rigidbodyaspect

#endif // RIGIDBODYASPECT_MOVINGBODY_H


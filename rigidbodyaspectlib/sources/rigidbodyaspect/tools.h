#ifndef TOOLS_H
#define TOOLS_H

#include "types.h"
#include "geometry/rbtypes.h"
#include "constants.h"
#include "backend/environmentbackend.h"



// stl
#include <variant>
#include <tuple>


namespace rigidbodyaspect::tools
{
  //using  planeProps2 = std::tuple<const blaze::DynamicMatrix<>&,const GM2Vector&,const GM2Vector&,const GM2Vector&,const GM2Vector&>;
  using  planeProps = std::tuple<const GM2Vector,const GM2Vector,const GM2Vector,const GM2Vector>;
  planeProps getPlaneProps(const rbtypes::FixedPlane& plane);

  using Spheres = std::vector<rbtypes::Sphere*>;
  using Planes = std::vector<rbtypes::FixedPlane*>;

  using MSphFPlCollisionObject = std::tuple<rbtypes::Sphere*,const rbtypes::FixedPlane*,seconds_type>;
  //using CollisionObject = std::tuple<Spheres,Planes,seconds_type>;

  enum class ColType {SpSp,SpPl};
  using CollisionObject = std::tuple<rbtypes::Sphere*, void* ,seconds_type,ColType>;
};

#endif // TOOLS_H

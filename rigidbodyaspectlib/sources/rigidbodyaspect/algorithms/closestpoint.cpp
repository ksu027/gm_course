#include "closestpoint.h"

namespace rigidbodyaspect::algorithms::closestpoint
{
  GM2Vector findClosePointOnPlane(const rbtypes::FixedPlane& plane, const GM2Vector& point)
  {
      auto u=0.0, v=0.0, du=1.0, dv=1.0;
      GM2Vector q_betta;

      while (du>constants::eps_double or dv>constants::eps_double)
      {

        const auto pl_eval = plane.evaluateLocal(rbtypes::FixedPlane::PSpacePoint{u,v},
                                                rbtypes::FixedPlane::PSizeArray{1UL,1UL});
        const auto Su = blaze::subvector<0UL,3UL>(pl_eval(1UL,0UL));
        const auto Sv = blaze::subvector<0UL,3UL>(pl_eval(0UL,1UL));

        const auto q_alpha = blaze::subvector<0UL,3UL>(pl_eval(0UL,0UL));

        //const auto n = blaze::normalize(blaze::cross(Su,Sv));

        auto SuSu = blaze::inner(Su,Su);
        auto SuSv = blaze::inner(Su,Sv);
        auto SvSv = blaze::inner(Sv,Sv);

        blaze::DynamicMatrix<double> A (2UL,2UL);
        A = {{SuSu,SuSv},{SuSv,SvSv}};

        //const auto p = sphere.frameOriginParent();
        const auto p = point;
        const auto d = q_alpha-p;
        //const auto d = p-q_alpha;

        auto dSu = blaze::inner(d,Su);
        auto dSv = blaze::inner(d,Sv);

        blaze::DynamicVector<double,blaze::columnVector> b {-dSu,-dSv};


        auto A_inv =blaze::inv(A);
        blaze::DynamicVector<double,blaze::columnVector> x =A_inv*b;

        du = x[0];
        dv = x[1];

        u += du;
        v += dv;

        q_betta=q_alpha;
        //std::cout<<"q_betta\n"<<q_betta<<'\n';
      };

      return q_betta;
  };
}

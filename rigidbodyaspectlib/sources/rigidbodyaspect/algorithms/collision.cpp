#include "collision.h"

#include <tuple>

using namespace std::chrono_literals;

namespace rigidbodyaspect::algorithms::collision
{
  detail::SphereFixedPlaneColliderReturnType
  detectCollision(const rbtypes::Sphere&     sphere,
                  const rbtypes::FixedPlane& plane)
  {
   const auto pl_eval = plane.evaluateParent(rbtypes::FixedPlane::PSpacePoint{0.0,0.0},
                                             rbtypes::FixedPlane::PSizeArray{1UL,1UL});
   const auto pl_u = blaze::subvector<0UL,3UL>(pl_eval(1UL,0UL));
   const auto pl_v = blaze::subvector<0UL,3UL>(pl_eval(0UL,1UL));

   const auto q = blaze::subvector<0UL,3UL>(pl_eval(0UL,0UL));
   const auto n = blaze::normalize(blaze::cross(pl_u,pl_v));

   const auto p = sphere.frameOriginParent();
   const auto r = sphere.m_radius;

   const auto d = (q+r*n)-p;
   const auto ds = sphere.m_ds;

   const auto dn = blaze::inner(d,n);
   const auto dsn = blaze::inner(ds,n);

   const auto x = dn/dsn;

   if (x>0.0 and x<=1.0)
   {
       return {detail::SphereFixedPlaneColliderStatus::Collision,x};
   }
   else
   {
       return {detail::SphereFixedPlaneColliderStatus::NoCollision,0};
   }
  }

  detail::SphereFixedPlaneColliderReturnType
  detectCollisionSpSp(const rbtypes::Sphere&     act_sphere,
                      const rbtypes::Sphere&     pas_sphere)
  {
    const auto p_act = act_sphere.frameOriginParent();
    const auto r_act = act_sphere.m_radius;
    const auto p_pas = pas_sphere.frameOriginParent();
    const auto r_pas = pas_sphere.m_radius;
    const auto r     = r_act+r_pas;

    const auto ds_act = act_sphere.m_ds;
    const auto ds_pas = pas_sphere.m_ds;

    const auto Q = p_act-p_pas;
    const auto R = ds_act-ds_pas;

    const auto QR = blaze::inner(Q,R);
    const auto RR = blaze::inner(R,R);
    const auto QQ = blaze::inner(Q,Q);

    const auto root = pow(QR,2)-RR*(QQ-pow(r,2));

    if (root < 0.0 )
    {
      return {detail::SphereFixedPlaneColliderStatus::NoCollision,0};
    }
    else
    {
      const auto x = (-QR-sqrt(root))/RR;
      if (x>0.0 and x<=1.0 )
      {
        return {detail::SphereFixedPlaneColliderStatus::Collision,x};
      }
      else
      {
        return {detail::SphereFixedPlaneColliderStatus::NoCollision,0};
      }
    }
  }



  std::vector<rigidbodyaspect::tools::CollisionObject> makeAllCollisionsVector(RigidBodyContainer& rigid_bodies, seconds_type dt, RigidBodyAspect& aspect)
  {
    std::vector<rigidbodyaspect::tools::CollisionObject> cols_vector;
    for (auto& sphere : rigid_bodies.spheres())
    {
      std::vector<rigidbodyaspect::tools::CollisionObject> temp_collision;
      /* set up sphere.cur_t = 0 */
      cashcompute::updateCurT(sphere,0ms);

      /* compute ds for all the states */
      cashcompute::updateDS(sphere,dt);

      /* adjust ds for slide */
      cashcompute::adjustDs(sphere,aspect);

      /* check state change */
      cashcompute::stateChangeGeneral(sphere,dt,aspect);

      if (cashcompute::isRestSp(sphere,aspect) or cashcompute::isSlideSp(sphere,aspect))
      {
        rbtypes::Sphere& att_sphere = aspect.spspSlide().contains(&sphere)? *aspect.spspSlide().value(&sphere) : *aspect.spspRest().value(&sphere);
        cashcompute::stateChangeSP(sphere,att_sphere,dt,aspect);
      }

      /* vector of all collisions */
      if (!(sphere.m_state==SphereState::Rest and aspect.spplRest().count(&sphere)>0))
      {
        temp_collision = findCollisions(sphere,dt,rigid_bodies,aspect);
        cols_vector.insert(cols_vector.end(),temp_collision.begin(),temp_collision.end());
      }
    }
    return cols_vector;
  };

  void deleteDuplicateAllCols(std::vector<rigidbodyaspect::tools::CollisionObject>& in_cols_vector, int i)
  {
    /* takes first sphere from the sphere set */
    SphereSet unique_spheres;
    if (i<int(in_cols_vector.size()))
    {
    const auto unique_end =
            std::remove_if(std::begin(in_cols_vector)+i,std::end(in_cols_vector),
                             [&unique_spheres](const rigidbodyaspect::tools::CollisionObject &col_obj) {
                               /* if collision sphere-plane check only one sphere */
                               if (std::get<3>(col_obj)==tools::ColType::SpPl)
                               {
                                 auto *sph = std::get<0>(col_obj);
                                 if (unique_spheres.count(sph)) return true;
                                 unique_spheres.insert(sph);
                                 return false;
                               }
                               else
                               {
                                 auto *sph_1 = std::get<0>(col_obj);
                                 auto *sph_2 = static_cast<rbtypes::Sphere*>(std::get<1>(col_obj));
                                 if (unique_spheres.count(sph_1) or unique_spheres.count(sph_2)) return true;
                                 unique_spheres.insert(sph_1);
                                 unique_spheres.insert(sph_2);
                                 return false;
                               }

                              }
                );
    in_cols_vector.erase(unique_end,std::end(in_cols_vector));
    }
  };

  std::vector<rigidbodyaspect::tools::CollisionObject> findCollisions(rbtypes::Sphere& sphere, const seconds_type& m_dt, RigidBodyContainer& rigid_bodies,RigidBodyAspect& aspect)
  {
    std::vector<rigidbodyaspect::tools::CollisionObject> collision_return;
    algorithms::collision::detail::SphereFixedPlaneColliderReturnType col_result;
    const auto count_t  = m_dt-sphere.m_cur_t;
      /* collisions Sphere Plane */
      for (auto& fixed_plane : rigid_bodies.fixedPlanes())
      {
       /* if sphere is attached to that plane, then do not check collisions with it */
       if ((aspect.spplSlide().value(&sphere)!=&fixed_plane) and
           (aspect.spplRest().value(&sphere)!=&fixed_plane))
       {
         col_result = algorithms::collision::detectCollision(sphere,fixed_plane);
         if (col_result.first == algorithms::collision::detail::SphereFixedPlaneColliderStatus::Collision)
         {
           /* insert only collisions of time > cur_t */
           if ((col_result.second*count_t).count()>constants::eps_double)
             collision_return.emplace_back(&sphere,&fixed_plane,(col_result.second*count_t+sphere.m_cur_t),tools::ColType::SpPl);
         }
       }
      }
      /* collisions Sphere Sphere  */
      for (auto& sphere_2 : rigid_bodies.spheres())
      {
        /* not itself and not sliding or resting */
        if (&sphere!=&sphere_2 and
           (aspect.spspSlide().value(&sphere)!=&sphere_2) and
           (aspect.spspRest().value(&sphere)!=&sphere_2))
        {
          col_result = algorithms::collision::detectCollisionSpSp(sphere,sphere_2);
          if (col_result.first == algorithms::collision::detail::SphereFixedPlaneColliderStatus::Collision)
          {
            /* insert only collisions of time > cur_t */
            if ((col_result.second*count_t).count()>constants::eps_double)
              collision_return.emplace_back(&sphere,&sphere_2,(col_result.second*count_t+sphere.m_cur_t),tools::ColType::SpSp);
          }
        }
      }
    return collision_return;
  };

}   // namespace rigidbodyaspect::algorithms::collision

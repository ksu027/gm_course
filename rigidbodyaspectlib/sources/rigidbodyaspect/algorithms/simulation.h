#ifndef RIGIDBODYASPECT_ALGORITHMS_SIMULATION_H
#define RIGIDBODYASPECT_ALGORITHMS_SIMULATION_H

#include "../geometry/movingbody.h"
#include "../cashcompute.h"
#include "../backend/rigidbodyaspect.h"

namespace rigidbodyaspect::algorithms::simulation
{

  void simulateGenericMovingBody(rbtypes::Sphere& sphere, seconds_type dt_sec, RigidBodyAspect& aspect);

}   // namespace rigidbodyaspect::algorithms::simulation


#endif   // RIGIDBODYASPECT_ALGORITHMS_SIMULATION_H

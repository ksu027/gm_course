#ifndef RIGIDBODYASPECT_ALGORITHMS_CLOSESTPOINT_H
#define RIGIDBODYASPECT_ALGORITHMS_CLOSESTPOINT_H

#include "../types.h"
#include "../tools.h"
#include "../geometry/rbtypes.h"
#include "../constants.h"
#include "../backend/environmentbackend.h"

// stl
#include <variant>


namespace rigidbodyaspect::algorithms::closestpoint
{
  GM2Vector findClosePointOnPlane(const rbtypes::FixedPlane& plane, const GM2Vector& point);
};

#endif // CLOSESTPOINT_H

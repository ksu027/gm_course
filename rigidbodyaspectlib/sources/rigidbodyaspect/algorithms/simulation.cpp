#include "simulation.h"



namespace rigidbodyaspect::algorithms::simulation
{

  void simulateGenericMovingBody(rbtypes::Sphere& sphere, seconds_type dt_sec, RigidBodyAspect& aspect)
  {
    // update physical properties
    sphere.translateParent(sphere.m_ds);
    /* update velocity*/
    if (sphere.m_state==rigidbodyaspect::SphereState::Free)
      cashcompute::updateVelocity(sphere,dt_sec);
    else if (sphere.m_state==rigidbodyaspect::SphereState::Slide and !cashcompute::isRestSp(sphere,aspect) and !cashcompute::isSlideSp(sphere,aspect))
    {
      sphere.m_velocity+=((sphere.m_ds/dt_sec.count()-sphere.m_velocity)/(dt_sec.count()*0.5))*dt_sec.count();
      sphere.m_velocity*=(0.9+sphere.m_friction*0.1);
    }
    else if (sphere.m_state==rigidbodyaspect::SphereState::Slide)
    {
      cashcompute::updateVelocity(sphere,dt_sec);
    }
  }

}

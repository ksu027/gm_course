#ifndef RIGIDBODYASPECT_ALGORITHMS_COLLISION_H
#define RIGIDBODYASPECT_ALGORITHMS_COLLISION_H

#include "../types.h"
#include "../tools.h"
#include "../geometry/rbtypes.h"
#include "../constants.h"
#include "../backend/environmentbackend.h"
#include "../backend/rigidbodyaspect.h"

// stl
#include <variant>


namespace rigidbodyaspect::algorithms::collision
{

  namespace detail
  {

    enum class SphereFixedPlaneColliderStatus {NoCollision,Collision,Parallel,Slide};


    //using ColliderReturnType = std::variant<CollisionObject, ColliderStatus>;
    using SphereFixedPlaneColliderReturnType = std::pair<SphereFixedPlaneColliderStatus,GM2UnitType>;

  }   // namespace detail

  detail::SphereFixedPlaneColliderReturnType
  detectCollision(const rbtypes::Sphere&     sphere,
                  const rbtypes::FixedPlane& plane);

  detail::SphereFixedPlaneColliderReturnType
  detectCollisionSpSp(const rbtypes::Sphere&     act_sphere,
                      const rbtypes::Sphere&     pas_sphere);

  std::vector<rigidbodyaspect::tools::CollisionObject>
       makeAllCollisionsVector(RigidBodyContainer& rigid_bodies, seconds_type dt, RigidBodyAspect& aspect);
  void deleteDuplicateAllCols(std::vector<rigidbodyaspect::tools::CollisionObject>& in_cols_vector, int i);

  std::vector<rigidbodyaspect::tools::CollisionObject>
       findCollisions(rbtypes::Sphere& sphere, const seconds_type& m_dt, RigidBodyContainer& rigid_bodies,RigidBodyAspect& aspect);
}   // namespace rigidbodyaspect::algorithms::collision


#endif   // RIGIDBODYASPECT_ALGORITHMS_COLLISION_H
